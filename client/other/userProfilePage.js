
Template.userProfilePage.onCreated(function() {
});

Template.userProfilePage.helpers({
  char () {
    return Meteor.user().profile.eveOnlineCharacterName.replace(" ", "_")
  },
  bids () {
    var b = Meteor.user().bids
    var a = AuctionItems.find({_id: {$in: b.map(x=>{return x.id})}}).fetch()
    a.map((x)=>{
      x.winning = !!b.filter(z=>{return z.id==x._id && z.value==x.topBid}).length
    }).sort((x,y)=>{
      var res
      if (x.winning == y.winning){
        return x.date-y.date
      } else {
        return x.winning
      }
    })
    return a
  },
  auctions(){
    var b = Meteor.user().auctions
    var d = new Date()
    d.setDate(d.getDate-5)
    var a = AuctionItems.find({_id: {$in: b.auctions}}).map(x=>{
      if (x.lastUpdateAuc < d){
        x.active == false
      }
    })
    return a
  },
  // apps () {
  //   return Applications.find({owner: this.userId})
  // }
  // corps() {
  //   return Object.keys(Meteor.user().roles)
  // }
})

Template.userProfilePage.events({
  "click .activate": function(event){
    Meteor.call("activate", event.target.attributes[1].value)
  },
  "click .disactivate": function(event){
    Meteor.call("disactivate", event.target.attributes[1].value)
  },
  "click .sellYourself": function(event){
    var a = Meteor.user().profile.eveOnlineCharacterName.replace(" ", "_")
      router.push({name: "auctionCreate", params: {name: a}})
  }
})
