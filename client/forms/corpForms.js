export default {
category: {
  type: "Select", value: "",
  label: "Category: ",
  allowedValues: [
    "High Security 1.0-0.6",
    "Low Security 0.5-0.1",
    "Null Security 0.0-negative",
    "Wormhole Space"
  ]},
  security: {
    type: "Select", value: "",
    label: "Security policy: ",
    allowedValues: [
      "Spies Welcome",
      "Risk Tolerant",
      "Risk Intolerant",
      "Strict Security"
    ]},
    activityattitude: {
      type: "Select", value: "",
      label: "Commitment attitude: ",
      allowedValues: [
        'Life Comes First',
        'On Call',
        'Moderate',
        'Duty Bound',
      ]}
      ,
      pvpgearing: {
        type: "Select", value: "",
        label: "Gearing towards Player vs. Player: ",
        allowedValues: [
          'Suicidal',
          'Risk Averse',
          'KB Oriented',
          'Uninterested'
        ]}
        ,
        primarytz: {
          type: "Select", value: "",
          label: "Primary TZ: ",
          allowedValues: [
            'US',
            'EU',
            'AU',
            'RU/Asia'
          ]}
          ,

          ////secondary,etc tz meant for WH corps with form enabling recruits
          ////to grasp level of activity while recognizing WH corps interest
          ////should include checkboxes or something for recruitment status

          secondarytz: {
            type: "Select", value: "",
            label: "Secondary TZ: ",
            allowedValues: [
              'US',
              'EU',
              'AU',
              'RU/Asia'
            ]}
            ,
            tertiarytz: {
              type: "Select", value: "",
              label: "Tertiary TZ: ",
              allowedValues: [
                'US',
                'EU',
                'AU',
                'RU/Asia'
              ]}
              ,
              quaternarytz: {
                type: "Select", value: "",
                label: "Quaternary TZ: ",
                allowedValues: [
                  'US',
                  'EU',
                  'AU',
                  'RU/Asia'
                ]}
                ,
                usrecruitment: {
                  type: "Select", value: "",
                  label: "US Recruitment Status: ",
                  allowedValues: [
                    'Wanting',
                    'Sensitive to TZ/Interested',
                    'Casual',
                    'Averse',
                    'Closed'
                  ]}
                  ,
                  rurecruitment: {
                    type: "Select", value: "",
                    allowedValues: [
                      'Wanting',
                      'Sensitive to TZ/Interested',
                      'Casual',
                      'Averse',
                      'Closed'
                    ]}
                    ,
                    eurecruitment: {
                      type: "Select", value: "",
                      label: "EU Recruitment Status/Interested: ",
                      allowedValues: [
                        'Wanting',
                        'Sensitive to TZ/Interested',
                        'Casual',
                        'Averse',
                        'Closed'
                      ]},
                      aurecruitment: {
                        type: "Select", value: "",
                        label: "AU Recruitment Status: ",
                        allowedValues: [
                          'Wanting',
                          'Sensitive to TZ/Interested',
                          'Casual',
                          'Averse',
                          'Closed'
                        ]}
                        ,
                        //SRP checkboxes for full ]}t), doctrinal
                        srpnonop: {
                          type: "Select", value: "",
                          label: "SRP for non-ops: ",
                          allowedValues: [
                            'Logistics',
                            'Doctrine',
                            'General',
                            'None'
                          ]},
                          srpop: {
                            type: "Select", value: "",
                            label: "SRP for ops: ",
                            allowedValues: [
                              'Logistics',
                              'Doctrine',
                              'General',
                              'None'
                            ]}
                            ,
                            opsattitude: {
                              type: "Select", value: "",
                              label: "Ops stature: ",
                              allowedValues: [
                                'Voluntery',
                                'Obligatory',
                                '100% tax',
                                'Disciplinary Awoxing'
                              ]},
                              incometax: {
                                type: "Select", value: "",
                                label: "Member taxation to further corporate wallet (not game engine taxation specific ai. taxing loot), indicates corporate sponsoring- SRP/Logistics/Infrastructure: ",
                                allowedValues: [
                                  'Unappetizing taxation',
                                  'Amicable corporate taxation',
                                  'Helpful/civic levels of taxation',
                                  'Nonexistent player subsistence of corp'
                                ]}
                                ,
                                //checkboxes for capitals and supercapital attitude
                                incomegeneration: {
                                  type: "Select", value: "",
                                  label: "Degree of corporate programs or attitude towards carebearing: ",
                                  allowedValues: [
                                    'Nurturing sponsorship of members',
                                    'Yielding to member subsistence',
                                    'Lukeworm to members',
                                    'Uninterested'
                                  ]}
                                }
