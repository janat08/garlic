//https://blog.meteor.com/create-a-simple-hello-world-app-with-meteor-and-apollo-64bab66a456f#.mq12ycxgk
import { db, Skill, Inv } from './connectors';
import Sequelize from 'sequelize';

// create the resolve functions for the available GraphQL queries
export default resolvers = {
  Skill: {
    requiredSkills(root, args) {
      return db.query(
        `SELECT
    typeName,
    it.typeId,
    COALESCE(skillLevel.valueFloat,
            skillLevel.valueInt) AS requiredLevel
FROM
    dgmTypeAttributes attr
        INNER JOIN
    invTypes it ON it.typeid = COALESCE(attr.valueint, attr.valuefloat)
        INNER JOIN
    dgmTypeAttributes skillLevel ON skillLevel.typeid = COALESCE(attr.valueint, attr.valuefloat)
WHERE
    attr.typeid = :id
        AND ((attr.attributeID = 182
        AND skillLevel.attributeID = 277)
        OR (attr.attributeID = 183
        AND skillLevel.attributeID = 278)
        OR (attr.attributeID = 184
        AND skillLevel.attributeID = 279)
        OR (attr.attributeID = 1285
        AND skillLevel.attributeID = 1286)
        OR (attr.attributeID = 1289
        AND skillLevel.attributeID = 1287)
        OR (attr.attributeID = 1290
        AND skillLevel.attributeID = 1288))`
        ,
      { replacements: {id: root.typeId}, type: Sequelize.QueryTypes.SELECT }
    )
    }
  },

  Query: {
    // skills(_, args){
    //   return Skill.findAll({where: args});
    // },
    ping(root, {message}, context) {
  return `Answering ${message}`
},
    skill(_, args){
      console.log(args.typeName)
      return Skill.findAll({where: {typeName: {$like: "%"+args.typeName+"%"}}})
      // .then((sdf)=> console.log(sdf));
    },
    hello(root, args, context) {
  return "Hello world!"
},
// findName(_, args) {
//   return db.query(`
//     QUERY MYSQL COMMAND WITH WHERE "%ARG%"
//     `, {replacements: args, type: Sequelize.QueryTypes.SELECT})
// },
    findSkill(_, args){
      return db.query(
        `SELECT
    typeName,
    it.typeId,
    COALESCE(skillLevel.valueFloat,
            skillLevel.valueInt) AS requiredLevel
FROM
    dgmTypeAttributes attr
        INNER JOIN
    invTypes it ON it.typeid = COALESCE(attr.valueint, attr.valuefloat)
        INNER JOIN
    dgmTypeAttributes skillLevel ON skillLevel.typeid = COALESCE(attr.valueint, attr.valuefloat)
WHERE
    attr.typeid = :id
        AND ((attr.attributeID = 182
        AND skillLevel.attributeID = 277)
        OR (attr.attributeID = 183
        AND skillLevel.attributeID = 278)
        OR (attr.attributeID = 184
        AND skillLevel.attributeID = 279)
        OR (attr.attributeID = 1285
        AND skillLevel.attributeID = 1286)
        OR (attr.attributeID = 1289
        AND skillLevel.attributeID = 1287)
        OR (attr.attributeID = 1290
        AND skillLevel.attributeID = 1288))`
        ,
      { replacements: args, type: Sequelize.QueryTypes.SELECT }
    )

  }
}
};
