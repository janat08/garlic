export default [
	{
		"typeID" : 33078,
		"typeName" : "Armor Layering",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24568,
		"typeName" : "Capital Remote Armor Repair Systems",
		"rank" : 10,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 27936,
		"typeName" : "Capital Remote Hull Repair Systems",
		"rank" : 10,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21803,
		"typeName" : "Capital Repair Systems",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22806,
		"typeName" : "EM Armor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22807,
		"typeName" : "Explosive Armor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3394,
		"typeName" : "Hull Upgrades",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22808,
		"typeName" : "Kinetic Armor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3392,
		"typeName" : "Mechanics",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 16069,
		"typeName" : "Remote Armor Repair Systems",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 27902,
		"typeName" : "Remote Hull Repair Systems",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3393,
		"typeName" : "Repair Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22809,
		"typeName" : "Thermal Armor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3363,
		"typeName" : "Corporation Management",
		"rank" : 1,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3368,
		"typeName" : "Diplomatic Relations",
		"rank" : 2,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3732,
		"typeName" : "Empire Control",
		"rank" : 5,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3731,
		"typeName" : "Megacorp Management",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 12241,
		"typeName" : "Sovereignty",
		"rank" : 8,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 23566,
		"typeName" : "Advanced Drone Avionics",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 12484,
		"typeName" : "Amarr Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 12487,
		"typeName" : "Caldari Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3437,
		"typeName" : "Drone Avionics",
		"rank" : 1,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 23618,
		"typeName" : "Drone Durability",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3442,
		"typeName" : "Drone Interfacing",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 12305,
		"typeName" : "Drone Navigation",
		"rank" : 1,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 23606,
		"typeName" : "Drone Sharpshooting",
		"rank" : 1,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3436,
		"typeName" : "Drones",
		"rank" : 1,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 24613,
		"typeName" : "Fighter Hangar Management",
		"rank" : 8,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 23069,
		"typeName" : "Fighters",
		"rank" : 12,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 12486,
		"typeName" : "Gallente Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3441,
		"typeName" : "Heavy Drone Operation",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 32339,
		"typeName" : "Heavy Fighters",
		"rank" : 12,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 43702,
		"typeName" : "Ice Harvesting Drone Operation",
		"rank" : 2,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 43703,
		"typeName" : "Ice Harvesting Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 24241,
		"typeName" : "Light Drone Operation",
		"rank" : 1,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 40572,
		"typeName" : "Light Fighters",
		"rank" : 12,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 33699,
		"typeName" : "Medium Drone Operation",
		"rank" : 2,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3438,
		"typeName" : "Mining Drone Operation",
		"rank" : 2,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 22541,
		"typeName" : "Mining Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 12485,
		"typeName" : "Minmatar Drone Specialization",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3439,
		"typeName" : "Repair Drone Operation",
		"rank" : 3,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 3440,
		"typeName" : "Salvage Drone Operation",
		"rank" : 4,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 23594,
		"typeName" : "Sentry Drone Interfacing",
		"rank" : 5,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 40573,
		"typeName" : "Support Fighters",
		"rank" : 12,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 27911,
		"typeName" : "Burst Projector Operation",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11579,
		"typeName" : "Cloaking",
		"rank" : 6,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3427,
		"typeName" : "Electronic Warfare",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19760,
		"typeName" : "Frequency Modulation",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19759,
		"typeName" : "Long Distance Jamming",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3435,
		"typeName" : "Propulsion Jamming",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3433,
		"typeName" : "Sensor Linking",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19761,
		"typeName" : "Signal Dispersion",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19766,
		"typeName" : "Signal Suppression",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19922,
		"typeName" : "Signature Focusing",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 27906,
		"typeName" : "Tactical Logistics Reconfiguration",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 4411,
		"typeName" : "Target Breaker Amplification",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19921,
		"typeName" : "Target Painting",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 19767,
		"typeName" : "Weapon Destabilization",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3434,
		"typeName" : "Weapon Disruption",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11207,
		"typeName" : "Advanced Weapon Upgrades",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3423,
		"typeName" : "Capacitor Emission Systems",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3418,
		"typeName" : "Capacitor Management",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3417,
		"typeName" : "Capacitor Systems Operation",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24572,
		"typeName" : "Capital Capacitor Emission Systems",
		"rank" : 10,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3426,
		"typeName" : "CPU Management",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3432,
		"typeName" : "Electronics Upgrades",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3424,
		"typeName" : "Energy Grid Upgrades",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3421,
		"typeName" : "Energy Pulse Weapons",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 28880,
		"typeName" : "Nanite Interfacing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 28879,
		"typeName" : "Nanite Operation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3413,
		"typeName" : "Power Grid Management",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 32797,
		"typeName" : "Resistance Phasing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 28164,
		"typeName" : "Thermodynamics",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3318,
		"typeName" : "Weapon Upgrades",
		"rank" : 2,
		"prisec" : "memory,perception"
	},
	{
		"typeID" : 20494,
		"typeName" : "Armored Command",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 11569,
		"typeName" : "Armored Command Specialist",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3354,
		"typeName" : "Command Burst Specialist",
		"rank" : 6,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 24764,
		"typeName" : "Fleet Command",
		"rank" : 12,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 20495,
		"typeName" : "Information Command",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3352,
		"typeName" : "Information Command Specialist",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3348,
		"typeName" : "Leadership",
		"rank" : 1,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 22552,
		"typeName" : "Mining Director",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 22536,
		"typeName" : "Mining Foreman",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3350,
		"typeName" : "Shield Command",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3351,
		"typeName" : "Shield Command Specialist",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3349,
		"typeName" : "Skirmish Command",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 11572,
		"typeName" : "Skirmish Command Specialist",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 43728,
		"typeName" : "Spatial Phenomena Generation",
		"rank" : 10,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 11574,
		"typeName" : "Wing Command",
		"rank" : 8,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 41404,
		"typeName" : "Capital Artillery Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41403,
		"typeName" : "Capital Autocannon Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41408,
		"typeName" : "Capital Beam Laser Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41405,
		"typeName" : "Capital Blaster Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20327,
		"typeName" : "Capital Energy Turret",
		"rank" : 7,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 21666,
		"typeName" : "Capital Hybrid Turret",
		"rank" : 7,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 21667,
		"typeName" : "Capital Projectile Turret",
		"rank" : 7,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41407,
		"typeName" : "Capital Pulse Laser Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41406,
		"typeName" : "Capital Railgun Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3316,
		"typeName" : "Controlled Bursts",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 24563,
		"typeName" : "Doomsday Operation",
		"rank" : 14,
		"prisec" : "intelligence,willpower"
	},
	{
		"typeID" : 41537,
		"typeName" : "Doomsday Rapid Firing",
		"rank" : 14,
		"prisec" : "intelligence,willpower"
	},
	{
		"typeID" : 3300,
		"typeName" : "Gunnery",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12203,
		"typeName" : "Large Artillery Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12209,
		"typeName" : "Large Autocannon Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12205,
		"typeName" : "Large Beam Laser Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12212,
		"typeName" : "Large Blaster Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3309,
		"typeName" : "Large Energy Turret",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3307,
		"typeName" : "Large Hybrid Turret",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3308,
		"typeName" : "Large Projectile Turret",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12215,
		"typeName" : "Large Pulse Laser Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12207,
		"typeName" : "Large Railgun Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12202,
		"typeName" : "Medium Artillery Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12208,
		"typeName" : "Medium Autocannon Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12204,
		"typeName" : "Medium Beam Laser Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12211,
		"typeName" : "Medium Blaster Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3306,
		"typeName" : "Medium Energy Turret",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3304,
		"typeName" : "Medium Hybrid Turret",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3305,
		"typeName" : "Medium Projectile Turret",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12214,
		"typeName" : "Medium Pulse Laser Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12206,
		"typeName" : "Medium Railgun Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3312,
		"typeName" : "Motion Prediction",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3310,
		"typeName" : "Rapid Firing",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3311,
		"typeName" : "Sharpshooter",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12201,
		"typeName" : "Small Artillery Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 11084,
		"typeName" : "Small Autocannon Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 11083,
		"typeName" : "Small Beam Laser Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12210,
		"typeName" : "Small Blaster Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3303,
		"typeName" : "Small Energy Turret",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3301,
		"typeName" : "Small Hybrid Turret",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3302,
		"typeName" : "Small Projectile Turret",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12213,
		"typeName" : "Small Pulse Laser Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 11082,
		"typeName" : "Small Railgun Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3315,
		"typeName" : "Surgical Strike",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 22043,
		"typeName" : "Tactical Weapon Reconfiguration",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3317,
		"typeName" : "Trajectory Analysis",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3322,
		"typeName" : "Auto-Targeting Missiles",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28073,
		"typeName" : "Bomb Deployment",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20212,
		"typeName" : "Cruise Missile Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3326,
		"typeName" : "Cruise Missiles",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3323,
		"typeName" : "Defender Missiles",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20312,
		"typeName" : "Guided Missile Precision",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 25718,
		"typeName" : "Heavy Assault Missile Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 25719,
		"typeName" : "Heavy Assault Missiles",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20211,
		"typeName" : "Heavy Missile Specialization",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3324,
		"typeName" : "Heavy Missiles",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20210,
		"typeName" : "Light Missile Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3321,
		"typeName" : "Light Missiles",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12441,
		"typeName" : "Missile Bombardment",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3319,
		"typeName" : "Missile Launcher Operation",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12442,
		"typeName" : "Missile Projection",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 21071,
		"typeName" : "Rapid Launch",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20209,
		"typeName" : "Rocket Specialization",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3320,
		"typeName" : "Rockets",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20314,
		"typeName" : "Target Navigation Prediction",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20213,
		"typeName" : "Torpedo Specialization",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3325,
		"typeName" : "Torpedoes",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20315,
		"typeName" : "Warhead Upgrades",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41410,
		"typeName" : "XL Cruise Missile Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 32435,
		"typeName" : "XL Cruise Missiles",
		"rank" : 7,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 41409,
		"typeName" : "XL Torpedo Specialization",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 21668,
		"typeName" : "XL Torpedoes",
		"rank" : 7,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3452,
		"typeName" : "Acceleration Control",
		"rank" : 4,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3450,
		"typeName" : "Afterburner",
		"rank" : 1,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 21603,
		"typeName" : "Cynosural Field Theory",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3453,
		"typeName" : "Evasive Maneuvering",
		"rank" : 2,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3451,
		"typeName" : "Fuel Conservation",
		"rank" : 2,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3454,
		"typeName" : "High Speed Maneuvering",
		"rank" : 5,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 21611,
		"typeName" : "Jump Drive Calibration",
		"rank" : 9,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3456,
		"typeName" : "Jump Drive Operation",
		"rank" : 5,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 21610,
		"typeName" : "Jump Fuel Conservation",
		"rank" : 8,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 24562,
		"typeName" : "Jump Portal Generation",
		"rank" : 14,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 4385,
		"typeName" : "Micro Jump Drive Operation",
		"rank" : 5,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3449,
		"typeName" : "Navigation",
		"rank" : 1,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 3455,
		"typeName" : "Warp Drive Operation",
		"rank" : 1,
		"prisec" : "intelligence,perception"
	},
	{
		"typeID" : 33407,
		"typeName" : "Advanced Infomorph Psychology",
		"rank" : 5,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3405,
		"typeName" : "Biology",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24606,
		"typeName" : "Cloning Facility Operation",
		"rank" : 10,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3411,
		"typeName" : "Cybernetics",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24242,
		"typeName" : "Infomorph Psychology",
		"rank" : 1,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 33399,
		"typeName" : "Infomorph Synchronizing",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 25538,
		"typeName" : "Neurotoxin Control",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25530,
		"typeName" : "Neurotoxin Recovery",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 2403,
		"typeName" : "Advanced Planetology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 2505,
		"typeName" : "Command Center Upgrades",
		"rank" : 4,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 2495,
		"typeName" : "Interplanetary Consolidation",
		"rank" : 4,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 2406,
		"typeName" : "Planetology",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 13279,
		"typeName" : "Remote Sensing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3396,
		"typeName" : "Advanced Industrial Ship Construction",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3388,
		"typeName" : "Advanced Industry",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3398,
		"typeName" : "Advanced Large Ship Construction",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24625,
		"typeName" : "Advanced Mass Production",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3397,
		"typeName" : "Advanced Medium Ship Construction",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3395,
		"typeName" : "Advanced Small Ship Construction",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22242,
		"typeName" : "Capital Ship Construction",
		"rank" : 14,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26224,
		"typeName" : "Drug Manufacturing",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3380,
		"typeName" : "Industry",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3387,
		"typeName" : "Mass Production",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3400,
		"typeName" : "Outpost Construction",
		"rank" : 16,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24268,
		"typeName" : "Supply Chain Management",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12180,
		"typeName" : "Arkonor Processing",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3410,
		"typeName" : "Astrogeology",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12181,
		"typeName" : "Bistot Processing",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12182,
		"typeName" : "Crokite Processing",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12183,
		"typeName" : "Dark Ochre Processing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11395,
		"typeName" : "Deep Core Mining",
		"rank" : 6,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25544,
		"typeName" : "Gas Cloud Harvesting",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12184,
		"typeName" : "Gneiss Processing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12185,
		"typeName" : "Hedbergite Processing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12186,
		"typeName" : "Hemorphite Processing",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 16281,
		"typeName" : "Ice Harvesting",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 18025,
		"typeName" : "Ice Processing",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 28585,
		"typeName" : "Industrial Reconfiguration",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12187,
		"typeName" : "Jaspet Processing",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12188,
		"typeName" : "Kernite Processing",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12189,
		"typeName" : "Mercoxit Processing",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3386,
		"typeName" : "Mining",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 22578,
		"typeName" : "Mining Upgrades",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12190,
		"typeName" : "Omber Processing",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12191,
		"typeName" : "Plagioclase Processing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12192,
		"typeName" : "Pyroxeres Processing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3385,
		"typeName" : "Reprocessing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3389,
		"typeName" : "Reprocessing Efficiency",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25863,
		"typeName" : "Salvaging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12193,
		"typeName" : "Scordite Processing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12196,
		"typeName" : "Scrapmetal Processing",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12194,
		"typeName" : "Spodumain Processing",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12195,
		"typeName" : "Veldspar Processing",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26253,
		"typeName" : "Armor Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26254,
		"typeName" : "Astronautics Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26255,
		"typeName" : "Drones Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26256,
		"typeName" : "Electronic Superiority Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26258,
		"typeName" : "Energy Weapon Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26259,
		"typeName" : "Hybrid Weapon Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26252,
		"typeName" : "Jury Rigging",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26260,
		"typeName" : "Launcher Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26257,
		"typeName" : "Projectile Weapon Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 26261,
		"typeName" : "Shield Rigging",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 13278,
		"typeName" : "Archaeology",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25811,
		"typeName" : "Astrometric Acquisition",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25810,
		"typeName" : "Astrometric Pinpointing",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 25739,
		"typeName" : "Astrometric Rangefinding",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3412,
		"typeName" : "Astrometrics",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21718,
		"typeName" : "Hacking",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3551,
		"typeName" : "Survey",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24624,
		"typeName" : "Advanced Laboratory Operation",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 23087,
		"typeName" : "Amarr Encryption Methods",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11444,
		"typeName" : "Amarr Starship Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11487,
		"typeName" : "Astronautic Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21790,
		"typeName" : "Caldari Encryption Methods",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11454,
		"typeName" : "Caldari Starship Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30324,
		"typeName" : "Defensive Subsystem Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11448,
		"typeName" : "Electromagnetic Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11453,
		"typeName" : "Electronic Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30326,
		"typeName" : "Electronic Subsystem Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30325,
		"typeName" : "Engineering Subsystem Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 23121,
		"typeName" : "Gallente Encryption Methods",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11450,
		"typeName" : "Gallente Starship Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11446,
		"typeName" : "Graviton Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11433,
		"typeName" : "High Energy Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11443,
		"typeName" : "Hydromagnetic Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3406,
		"typeName" : "Laboratory Operation",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11447,
		"typeName" : "Laser Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11452,
		"typeName" : "Mechanical Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3409,
		"typeName" : "Metallurgy",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21791,
		"typeName" : "Minmatar Encryption Methods",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11445,
		"typeName" : "Minmatar Starship Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11529,
		"typeName" : "Molecular Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11442,
		"typeName" : "Nanite Engineering",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11451,
		"typeName" : "Nuclear Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30327,
		"typeName" : "Offensive Subsystem Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11441,
		"typeName" : "Plasma Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30788,
		"typeName" : "Propulsion Subsystem Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11455,
		"typeName" : "Quantum Physics",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3403,
		"typeName" : "Research",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12179,
		"typeName" : "Research Project Management",
		"rank" : 8,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 11449,
		"typeName" : "Rocket Science",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3402,
		"typeName" : "Science",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24270,
		"typeName" : "Scientific Networking",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3408,
		"typeName" : "Sleeper Encryption Methods",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21789,
		"typeName" : "Sleeper Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 23123,
		"typeName" : "Takmahl Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 20433,
		"typeName" : "Talocan Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 23124,
		"typeName" : "Yan Jung Technology",
		"rank" : 5,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 24571,
		"typeName" : "Capital Shield Emission Systems",
		"rank" : 10,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21802,
		"typeName" : "Capital Shield Operation",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12365,
		"typeName" : "EM Shield Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12367,
		"typeName" : "Explosive Shield Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 44067,
		"typeName" : "Invulnerability Core Operation",
		"rank" : 8,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 12366,
		"typeName" : "Kinetic Shield Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 21059,
		"typeName" : "Shield Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3422,
		"typeName" : "Shield Emission Systems",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3419,
		"typeName" : "Shield Management",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3416,
		"typeName" : "Shield Operation",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3425,
		"typeName" : "Shield Upgrades",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3420,
		"typeName" : "Tactical Shield Manipulation",
		"rank" : 4,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 11566,
		"typeName" : "Thermal Shield Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3359,
		"typeName" : "Connections",
		"rank" : 3,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3361,
		"typeName" : "Criminal Connections",
		"rank" : 3,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3357,
		"typeName" : "Diplomacy",
		"rank" : 1,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3894,
		"typeName" : "Distribution Connections",
		"rank" : 2,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3358,
		"typeName" : "Fast Talk",
		"rank" : 4,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3893,
		"typeName" : "Mining Connections",
		"rank" : 2,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3356,
		"typeName" : "Negotiation",
		"rank" : 2,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3895,
		"typeName" : "Security Connections",
		"rank" : 2,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 3355,
		"typeName" : "Social",
		"rank" : 1,
		"prisec" : "charisma,intelligence"
	},
	{
		"typeID" : 20342,
		"typeName" : "Advanced Spaceship Command",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33095,
		"typeName" : "Amarr Battlecruiser",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3339,
		"typeName" : "Amarr Battleship",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 24311,
		"typeName" : "Amarr Carrier",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3335,
		"typeName" : "Amarr Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33091,
		"typeName" : "Amarr Destroyer",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20525,
		"typeName" : "Amarr Dreadnought",
		"rank" : 12,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20524,
		"typeName" : "Amarr Freighter",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3331,
		"typeName" : "Amarr Frigate",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3343,
		"typeName" : "Amarr Industrial",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30650,
		"typeName" : "Amarr Strategic Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 34390,
		"typeName" : "Amarr Tactical Destroyer",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3347,
		"typeName" : "Amarr Titan",
		"rank" : 16,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12095,
		"typeName" : "Assault Frigates",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28656,
		"typeName" : "Black Ops",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33096,
		"typeName" : "Caldari Battlecruiser",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3338,
		"typeName" : "Caldari Battleship",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 24312,
		"typeName" : "Caldari Carrier",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3334,
		"typeName" : "Caldari Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33092,
		"typeName" : "Caldari Destroyer",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20530,
		"typeName" : "Caldari Dreadnought",
		"rank" : 12,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20526,
		"typeName" : "Caldari Freighter",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3330,
		"typeName" : "Caldari Frigate",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3342,
		"typeName" : "Caldari Industrial",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30651,
		"typeName" : "Caldari Strategic Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 35680,
		"typeName" : "Caldari Tactical Destroyer",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3346,
		"typeName" : "Caldari Titan",
		"rank" : 16,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28374,
		"typeName" : "Capital Industrial Ships",
		"rank" : 12,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20533,
		"typeName" : "Capital Ships",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 37615,
		"typeName" : "Command Destroyers",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 23950,
		"typeName" : "Command Ships",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12093,
		"typeName" : "Covert Ops",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28615,
		"typeName" : "Electronic Attack Ships",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 22551,
		"typeName" : "Exhumers",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33856,
		"typeName" : "Expedition Frigates",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33097,
		"typeName" : "Gallente Battlecruiser",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3336,
		"typeName" : "Gallente Battleship",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 24313,
		"typeName" : "Gallente Carrier",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3332,
		"typeName" : "Gallente Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33093,
		"typeName" : "Gallente Destroyer",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20531,
		"typeName" : "Gallente Dreadnought",
		"rank" : 12,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20527,
		"typeName" : "Gallente Freighter",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3328,
		"typeName" : "Gallente Frigate",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3340,
		"typeName" : "Gallente Industrial",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30652,
		"typeName" : "Gallente Strategic Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 35685,
		"typeName" : "Gallente Tactical Destroyer",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3344,
		"typeName" : "Gallente Titan",
		"rank" : 16,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 16591,
		"typeName" : "Heavy Assault Cruisers",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28609,
		"typeName" : "Heavy Interdiction Cruisers",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 29637,
		"typeName" : "Industrial Command Ships",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12092,
		"typeName" : "Interceptors",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12098,
		"typeName" : "Interdictors",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 29029,
		"typeName" : "Jump Freighters",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 12096,
		"typeName" : "Logistics Cruisers",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 40328,
		"typeName" : "Logistics Frigates",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 28667,
		"typeName" : "Marauders",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 17940,
		"typeName" : "Mining Barge",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 32918,
		"typeName" : "Mining Frigate",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33098,
		"typeName" : "Minmatar Battlecruiser",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3337,
		"typeName" : "Minmatar Battleship",
		"rank" : 8,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 24314,
		"typeName" : "Minmatar Carrier",
		"rank" : 14,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3333,
		"typeName" : "Minmatar Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 33094,
		"typeName" : "Minmatar Destroyer",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20532,
		"typeName" : "Minmatar Dreadnought",
		"rank" : 12,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 20528,
		"typeName" : "Minmatar Freighter",
		"rank" : 10,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3329,
		"typeName" : "Minmatar Frigate",
		"rank" : 2,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3341,
		"typeName" : "Minmatar Industrial",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30653,
		"typeName" : "Minmatar Strategic Cruiser",
		"rank" : 5,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 34533,
		"typeName" : "Minmatar Tactical Destroyer",
		"rank" : 3,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3345,
		"typeName" : "Minmatar Titan",
		"rank" : 16,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 34327,
		"typeName" : "ORE Freighter",
		"rank" : 9,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3184,
		"typeName" : "ORE Industrial",
		"rank" : 4,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 22761,
		"typeName" : "Recon Ships",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3327,
		"typeName" : "Spaceship Command",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 19719,
		"typeName" : "Transport Ships",
		"rank" : 6,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 11584,
		"typeName" : "Anchoring",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3373,
		"typeName" : "Starbase Defense Management",
		"rank" : 7,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 37797,
		"typeName" : "Structure Doomsday Operation",
		"rank" : 2,
		"prisec" : "memory,willpower"
	},
	{
		"typeID" : 37798,
		"typeName" : "Structure Electronic Systems",
		"rank" : 2,
		"prisec" : "memory,willpower"
	},
	{
		"typeID" : 37799,
		"typeName" : "Structure Engineering Systems",
		"rank" : 2,
		"prisec" : "memory,willpower"
	},
	{
		"typeID" : 37796,
		"typeName" : "Structure Missile Systems",
		"rank" : 2,
		"prisec" : "memory,willpower"
	},
	{
		"typeID" : 30532,
		"typeName" : "Amarr Defensive Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30536,
		"typeName" : "Amarr Electronic Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30539,
		"typeName" : "Amarr Engineering Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30537,
		"typeName" : "Amarr Offensive Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30538,
		"typeName" : "Amarr Propulsion Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30544,
		"typeName" : "Caldari Defensive Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30542,
		"typeName" : "Caldari Electronic Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30548,
		"typeName" : "Caldari Engineering Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30549,
		"typeName" : "Caldari Offensive Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30552,
		"typeName" : "Caldari Propulsion Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30540,
		"typeName" : "Gallente Defensive Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30541,
		"typeName" : "Gallente Electronic Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30546,
		"typeName" : "Gallente Engineering Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30550,
		"typeName" : "Gallente Offensive Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30553,
		"typeName" : "Gallente Propulsion Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30545,
		"typeName" : "Minmatar Defensive Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30543,
		"typeName" : "Minmatar Electronic Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30547,
		"typeName" : "Minmatar Engineering Systems",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 30551,
		"typeName" : "Minmatar Offensive Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 30554,
		"typeName" : "Minmatar Propulsion Systems",
		"rank" : 1,
		"prisec" : "perception,willpower"
	},
	{
		"typeID" : 3430,
		"typeName" : "Advanced Target Management",
		"rank" : 3,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 33000,
		"typeName" : "Gravimetric Sensor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 33001,
		"typeName" : "Ladar Sensor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3428,
		"typeName" : "Long Range Targeting",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 32999,
		"typeName" : "Magnetometric Sensor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 33002,
		"typeName" : "Radar Sensor Compensation",
		"rank" : 2,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3431,
		"typeName" : "Signature Analysis",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 3429,
		"typeName" : "Target Management",
		"rank" : 1,
		"prisec" : "intelligence,memory"
	},
	{
		"typeID" : 16622,
		"typeName" : "Accounting",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3446,
		"typeName" : "Broker Relations",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 25235,
		"typeName" : "Contracting",
		"rank" : 1,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 25233,
		"typeName" : "Corporation Contracting",
		"rank" : 3,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 33467,
		"typeName" : "Customs Code Expertise",
		"rank" : 2,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 16595,
		"typeName" : "Daytrading",
		"rank" : 1,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 16597,
		"typeName" : "Margin Trading",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 16598,
		"typeName" : "Marketing",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 16594,
		"typeName" : "Procurement",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3444,
		"typeName" : "Retail",
		"rank" : 2,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 3443,
		"typeName" : "Trade",
		"rank" : 1,
		"prisec" : "charisma,willpower"
	},
	{
		"typeID" : 18580,
		"typeName" : "Tycoon",
		"rank" : 6,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 3447,
		"typeName" : "Visibility",
		"rank" : 3,
		"prisec" : "charisma,memory"
	},
	{
		"typeID" : 16596,
		"typeName" : "Wholesale",
		"rank" : 4,
		"prisec" : "charisma,memory"
	}
]
