// let myJobs = new JobCollection('queue', {
//    idGeneration: 'MONGO',
//    transform(d) {
//       try {
//          var res = new Job(myJobs, d);
//       } catch (e) {
//          var res = d;
//        }
//       return res;
//     }
//  }
// );
//
// let { later } = myJobs;
//
// if (Meteor.isClient) {
//
//    let tick = 2500;
//
//    let stats = new Mongo.Collection('jobStats');
//    Meteor.subscribe('clientStats');
//
//    let jobsProcessed = new ReactiveVar(0);
//    let reactiveWindowWidth = new ReactiveVar(0);
//
//    Meteor.startup(function() {
//       let timeout = null;
//       reactiveWindowWidth.set($(window).width());
//       return $(window).resize(function() {
//          if (timeout) { Meteor.clearTimeout(timeout); }
//          return timeout = Meteor.setTimeout(function() {
//                timeout = null;
//                return reactiveWindowWidth.set($(window).width());
//              }
//             , 100);
//       });
//    });
//
//    let parseState = new ReactiveVar("");
//    let parseSched = new ReactiveVar([]);
//    let reactiveDate = new ReactiveVar(new Date());
//    let localWorker = new ReactiveVar(null);
//
//    Meteor.setInterval((() => reactiveDate.set(new Date())), tick);
//
//    let q = null;
//    let myType = 'testJob_null';
//
//    let timeFormatter = function(time) {
//       let now = reactiveDate.get();
//       if (Math.abs(time - now) < tick) {
//          return "Now";
//       } else {
//          return moment(time).from(now);
//        }
//     };
//  }
//
//
// if (Meteor.isServer) {
//
//    // myJobs.setLogStream process.stdout
//    myJobs.promote(5000);
//
//    Meteor.startup(function() {
//
//       // Don't allow users to modify the user docs
//       Meteor.users.deny({update() { return true; } });
//
//       myJobs.startJobServer();
//
//       let jobsProcessed = 0;
//       let clientsSeen = {};
//       let currentClients = {};
//       let publishStatsChanges = function() {
//          for (let c in currentClients) { let f = currentClients[c];          f(); }
//          return null;
//        };
//
//       Meteor.publish('clientStats', function() {
//          currentClients[this.connection.id] = () => {
//             return this.changed('jobStats', 'stats', {
//                jobsProcessed,
//                clientsSeen: Object.keys(clientsSeen).length,
//                currentClients: Object.keys(currentClients).length
//              }
//             );
//           };
//          this.onStop(() => {
//             delete currentClients[this.connection.id];
//             return publishStatsChanges();
//           }
//          );
//          this.added('jobStats', 'stats', {
//             jobsProcessed,
//             clientsSeen: Object.keys(clientsSeen).length,
//             currentClients: Object.keys(currentClients).length
//           }
//          );
//          this.ready();
//          return publishStatsChanges();
//        }
//       );
//
//       myJobs.events.on('jobDone', function(msg) {
//          if (!msg.error && !!msg.connection) {
//             jobsProcessed++;
//             if (clientsSeen[msg.connection.id] == null) { clientsSeen[msg.connection.id] = 0; }
//             clientsSeen[msg.connection.id]++;
//             return publishStatsChanges();
//           }
//        }
//       );
//
//       Meteor.publish('allJobs', function(clientUserId) {
//          // This prevents a race condition on the client between Meteor.userId() and subscriptions to this publish
//          // See: https://stackoverflow.com/questions/24445404/how-to-prevent-a-client-reactive-race-between-meteor-userid-and-a-subscription/24460877#24460877
//          if (this.userId === clientUserId) {
//             let suffix = this.userId ? `_${this.userId.substr(0,5)}` : "";
//             return myJobs.find({ type: `testJob${suffix}`, 'data.owner': this.userId });
//          } else {
//             return [];
//           }
//        }
//       );
//
//       myJobs.events.on('error', msg => console.warn(`${new Date()}, ${msg.userId}, ${msg.method}, ${msg.error}\n`)
//       );
//
//       // Only allow job owners to manage or rerun jobs
//       myJobs.allow({
//          manager(userId, method, params) {
//             let ids = params[0];
//             if (typeof ids !== 'object' || !(ids instanceof Array)) {
//                ids = [ ids ];
//              }
//             let numIds = ids.length;
//             let numMatches = myJobs.find({ _id: { $in: ids }, 'data.owner': userId }).count();
//             return numMatches === numIds;
//           },
//
//          jobRerun(userId, method, params) {
//             let id = params[0];
//             let numMatches = myJobs.find({ _id: id, 'data.owner': userId }).count();
//             return numMatches === 1;
//           },
//
//          jobSave(userId, method, params) {
//             let doc = params[0];
//             return doc.data.owner === userId;
//           },
//
//          getWork(userId, method, params) {
//             let suffix = userId ? `_${userId.substr(0,5)}` : "";
//             return params[0][0] === `testJob${suffix}` && params[0].length === 1;
//           },
//
//          worker(userId, method, params) {
//             if (method === 'getWork') {
//                return false;
//             } else {
//                let id = params[0];
//                let numMatches = myJobs.find({ _id: id, 'data.owner': userId }).count();
//                return numMatches === 1;
//              }
//           }
//       });
//
//       new Job(myJobs, 'cleanup', {})
//          .repeat({ schedule: myJobs.later.parse.text("every 5 minutes") })
//          .save({cancelRepeats: true});
//
//       let q = myJobs.processJobs('cleanup', { pollInterval: false, workTimeout: 60*1000 }, function(job, cb) {
//          let current = new Date();
//          current.setMinutes(current.getMinutes() - 5);
//          let ids = [].map(d => d._id);
//          if (ids.length > 0) { myJobs.removeJobs(ids); }
//          // console.warn "Removed #{ids.length} old jobs"
//          job.done(`Removed ${ids.length} old jobs`);
//          return cb();
//        }
//       );
//
//       return myJobs.find({ type: 'cleanup', status: 'ready' })
//          .observe({
//             added() {
//                return q.trigger();
//              }
//       });
//    });
//  }
