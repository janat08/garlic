import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import Swagger from 'swagger-client'
// import swaggerclient from 'warehouseman:meteor-swagger-client';

Meteor.methods({
  //follow the thread > post methods to create filter groups
  'filters.create': function(name) {

    let _id = AuctionSkillFilters.insert({
      name,
      created: new Date(),
    })

    // return _id;
    //return id to access it's properties, as in posts of a thread
  },

  'application.create': function(data) {
    Swag("Skills/get_characters_character_id_skills", {character_id: data.main}, Meteor.user()).catch(x=>{
      throw new Meteor.Error(403, "login into Eve Online SSO with your main character")
    })

    HomeCounts.upsert({name: "applications"}, {$inc: {count: 1}})

    return Applications.insert({
      ...data,
      owner: this.userId,
      addressees: [],
      concluded: false,
      awaiting: false,
      //age of application/updating chars
      start: new Date(),
      limit: 20, //imposed allowed number of recepients
      length: 0, //number of current evaluations by the recepients
      charScan: new Date(),
    })
  },
  // https://support.eveonline.com/hc/en-us/articles/203217712-Roles-Listing
  // LIST OF ROLES IN CORP: UNDER CharsInfo.corporationRoles.[1/2 etc is in fact object]
  //auditor kick
  //Recruiter accept
  //Fitting Manager is fc

  'corp.apply': function (appId, corpName) {
    var corp = Corps.findOne({name: corpName})
    var app = Applications.findOne({_id: appId})
    console.log(corp, app,"id", appId, corpName)

    if (corp.limit < app.length) {
      throw new Meteor.Error(403, "Corp doesn't accept applications with more than ${corp.limit} pending recepients, you got ${app.length}");
    }

    if (app.limit> corp.limit) {
      Applications.update(
        {_id: app._id},
        //start of application at corp, also start of application
        { $push: { addressees: {name: corp.name, status: "", applied: new Date()} },
        $set: {limit: corp.limit},
        $inc: {length: 1}
      }
    )
  } else {
    Applications.update(
      {_id: app._id},
      //start of application at corp, also start of application
      { $push: { addressees: {name: corp.name, status: "", applied: new Date()} },
      $inc: {length: 1}
    }
  )
}
},


'corp.accept': function (app, corp, owner) {
  if (!user || !Roles.userIsInRole(user, ['Director', 'CEO', 'Recruiter'], corp)) {
    throw new Meteor.Error(403, "Access denied")
  }

  var user = Meteor.user()
  var a = []
  Meteor.user().chars.forEach((x)=>{
    a.push(x.name)
  })
  var redactedUser = {
    roles: this.userId,
    chars: a
  }

  // Roles.addUsersToRoles(owner, ["Member"], corp);



  //MAKE this update it's own method with param for status
  Applications.update(
    {_id: app, "addressees.name": corp},
    { $set: {recruiter: redactedUser, awaiting: true, "addressees.$.status": true }
    // ,{ $pull: { addressees: { name: { $ne: corp } } } }
  }
)

if (Meteor.isServer) {
  var start = Applications.findOne({_id: app})
  var ind = start.addresses.reduce((a,v,i)=>{return a = v.name==corp?i:0},0)
  var date = new Date()
  var period = date - start.addresses[ind].applied
  var corp = Corps.findOne({name: corp})

  var x = 0
  var date30 = new Date().setDate(-30)
  while (corp.total[x].date<date30){
    x++
  }
  corp.total.splice(0, x)
  var average = (corp.total.reduce((a, x)=>{return a+x.period}, 0)+date - start.addresses[ind].applied)/(corp.total.length+1)

  Corps.update({name: corp}, {$set: {averageDate: average}, $push: {total: {date: date, period: period}, accepted: {date: date, period: period}}})
  DeniedCorp.update({year: a.getFullYear(), month: a.getMonth(), day: a.getDate()}, {$inc: {total: 1, accepted: 1}})
}
// Applications.update(
//   { _id: app },
//   { $pull: { addressees: { name: { $ne: corp } } } }
// )
// Applications.update({_id: app}, {$set: {concluded: true, recepient: corp}})
return
},


'corp.decline': function (app, corp) {
  //Click aggregator for denial rate
  //click aggregator for acceptance rate

  if (!user || !Roles.userIsInRole(user, ["Recruiter"], corp)) {
    throw new Meteor.Error(403, "Access denied")
  }


  var a = []
  Meteor.user().chars.forEach((x)=>{
    a.push(x.name)
  })
  var redactedUser = {
    roles: this.userId,
    chars: a
  }

  Applications.update(
    {_id: app, "addressees.name": corp},
    { $set: { "addressees.$.status": false, "addressees.$.recruiter": redactedUser }, $inc: {length: -1}}
  )

  if (Meteor.isServer) {
    var start = Applications.findOne({_id: app})
    var ind = start.addresses.reduce((a,v,i)=>{return a = v.name==corp?i:0},0)
    var corp = Corps.findOne({name: corp})
    var date = new Date()
    var x = 0
    var date30 = new Date().setDate(-30)
    while (corp.total[x].date<date30){
      x++
    }
    corp.total.splice(0, x)
    var average = (corp.total.reduce((a, x)=>{return a+x.period}, 0)+date - start.addresses[ind].applied)/(corp.total.length+1)

    Corps.update({name: corp}, {$set: {averageDate: average}, $push: {total: {date: date, period: date - start.addresses[ind].applied}}})
    DeniedCorp.update({year: a.getFullYear(), month: a.getMonth(), day: a.getDate()}, {$inc: {total: 1}})
  }


  return
},
async calll() {
  return "asdfasdf"
}
});

//chars might have to array of numbers instead
export const checkAppCharCorps = new ValidatedMethod({
  name: 'app.check.app.char.corps',
  validate: new SimpleSchema({
    chars: {type: Array, optional: true},
    //[String]
  }).validator(),
  run({ chars }) {
    // const swaggerSpecURL = "http://petstore.swagger.io/v2/swagger.json";
    // SwaggerClients.addRemoteHost(swaggerSpecURL, "shit");
    // const host = SwaggerClients.getHost("shit");
    // console.log(host, SwaggerClients)
    // pet = host.sync.pet.getPetById({id:1}, hdrs);
    // console.log(pet, "//////////////////////////////")
    // Shite.insert({pet})
    return "aaaaaaaaaaaaa"
    // HTTP.get("https://esi.tech.ccp.is/latest/characters/95465499",
    // // params: {character_id: 95465499}},
    // (error, result)=> {
    //   if (result) {
    //     console.log(result.data, result.content, result )
    //   }
    // })

  }
})
// Meteor.http.call("GET", "https://esi.tech.ccp.is/latest/characters/95465499/", {headers: {from: "jey.and.key@gmail.com", host: "garlic"}} (error, result)=> {
//   if (result) {
//     console.log(result.data, result.content, )
//   }
// })
// const swaggerSpecURL = "http://petstore.swagger.io/v2/swagger.json";
// SwaggerClients.addRemoteHost(swaggerSpecURL, "shit");
// const host = SwaggerClients.getHost("shit");
// console.log(host, SwaggerClients)
// pet = host.sync.pet.getPetById({id:1}, hdrs);
// console.log(pet, "//////////////////////////////")
// Shite.insert({pet})

export const checkAppSkillCompletion = new ValidatedMethod({
  name: 'app.check.skill.completion',
  validate: new SimpleSchema({
    chars: { type: Array},
  }).validator(),
  run({ chars }) {
    if (Meteor.isClient) {
      HTTP.get
    }
  }
})
