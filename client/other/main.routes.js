import {subsCache} from "./main.js"
import auctionPage from '/imports/ui/auctionPage.vue';
import filters from '/client/auction/filters.vue';

export default [
    {
        path: '/',
        name: 'home',
        component: '/imports/ui/homePage.vue'
    },
    {
        path: '/profile',
        name: 'profile',
        component: '/imports/ui/userProfilePage.vue'
        // Nested routes
    },
    {
        path: '/auction',
        name: 'auction',
        // component: '/imports/ui/auctionPage.vue',
        components: {
            default: auctionPage,
            sidebar: filters,
        },
    },
    {
        path: '/auction/create/:name',
        name: 'auctionCreate',
        component: '/client/forms/auctionCreate.vue'
    },
    {
        path: '/auction/item/:charName', //CHANGE TO NAME AND ENABLE HISTORY (POP-UP)
        name: 'auctionItem',
        component: '/client/auction/charInfo.vue',
        props: true,
        // beforeEnter: (to, from, next) => {
        //   //calls next, but find doesn't resolve before then
        //   //use global guard
        //   subsCache.onReady(next)
        // }
    },
    {
        path: '/auction/archive',
        name: 'auctionArchive',
        component: '/imports/ui/auctionArchive.vue'
    },
    {
        path: '/corporate/list',
        name: 'corporateList',
        component: '/client/corp/corporateList.vue'
    },
    {
        path: '/corporate/home/:name',
        name: 'corporateHome',
        component: '/client/corp/corporateHome.vue'
    },
    {
        path: '/corporate/create',  //maybe dynamic?
        name: 'corporateCreate',
        component: '/client/forms/corporateCreate.vue'
    },
    // { DUPLICATE
    // path: '/application/create',
    //   name: 'applicationCreate',
    //   component: '/client/forms/applicationCreate.vue'
    // },
    //   'corporate/edit/:id' {
    //     name: 'page2',
    //     component: '/imports/ui/corporateEdit'
    //   }
    {
        path: '/corporate/application',
        name: 'applicationCreate',
        component: '/client/forms/applicationCreate.vue'
    },
];
