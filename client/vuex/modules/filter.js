const sortFields = ['rate', 'deviation', 'date'];
//
const state = {
  selectedFilters: [{name: "ike", selected: false, filters: [{name: "Gunnery", levels: [4]}, {name: "Planetology", levels: [5]}]}], //array of types
  newGroup: [],
  groups: [{name: "ike", selected: false, filters: [{name: "Gunnery", levels: []}, {name: "Planetology", levels: []}]}],
  // collection: [],
  newSkills: [],
  skills: ['jack1', 'jack2', 'jack3', 'jack4', 'jack5'],
  chars: [
    {name: 'yoda',
    skills: ['jack1', 'jack2', 'jack3']
  }, {
    name: "kek",
    skills: ['jack3', 'jack4']
  },{
    name: 'stupid',
    skills: ['jack4', 'jack5']
  }],
  sortDirection: -1,
  sortField: sortFields[0],
};

const getters = {
  selectedFilters: sate => state.selectedFilters,
  sortDirection: state => state.sortDirection,
  sortField: state => state.sortField,
  sortedChars: state=> state.chars
  // sortedChars: state => {
  //   //can't return forEach as it's not bound to actual arraynp
  //   // state.chars.forEach((element) => {
  //   return state.chars.map((char) => {
  //     // element.skills.map(function (char) {
  //     return char.skills.map((skill) => {
  //       return skill.match = state.filters(key => skill == key).length == 0?
  //       false
  //       :true
  //     })
  //   })
  // }
  };

  const mutations = {
    toggle_select(state, pl) {
      //should actually check for conflicts
      //unite filters
      state.groups[pl.gIndex].selected?
      state.selectedFilters.push(state.groups[pl.gIndex]):
      state.selectedFilters.splice(
      state.selectedFilters.indexOf(state.groups[pl.gIndex])
      // state.selectedFilters.reduce((a, v, i) => {
      // return state.groups[gIndex].name == v.name? a = i : a;
      // }, null)
      , 1)
      state.groups[pl.gIndex].selected = !state.groups[pl.gIndex].selected
    },
    // FILTER_UNSELECT(state, filter) {
    //   state.selectedFilters =
    //   state.selectedFilters.filter(function(value) {
    //     return value != filter
    //   })
    // },
    submit_group (state, name, filters) {
      var kek = {}
      kek.newSkills = state.newGroup
      kek.name = name
      kek.selected = false
      state.groups.push(kek)
      state.newGroup = []
    },
    rename_group (state, pl) {
      state.groups[pl.index].name = pl.a
    },
    upsert_skill (state, pl) {
      //acts on behalf of skill viewer too
      state.groups[pl.g].filters[pl.f] = pl.o
    },
    add_filter (state, text) {
      var text = {
        name: text,
        levels: []
      }
      state.newGroup.push(
        text
      )
    },
    delete_filter (state, filter) {
      state.filters.splice(state.filters.indexOf(filter), 1)
    },
  };

  const actions = {
    // selectFilters({store}, id) {
    //   // state is immutable
    //   store.dispatch('FORUM_SELECTED_THREAD_ID', id);
    // },
    // createFilter(_, name) {
    //   return this.callMethod('filters.create', name, (err, result) => {
    //     if(err) {
    //       console.error(err);
    //     } else {
    //       // Call another action on the submodule
    //       this.actions.selectFilter(result);
    //     }
    //   });
    // },
    removeFilter (state) {
          return this.callMethod('filters.remove', state.selectedFilterId);
        },
        createFilter (_ , msg) {
          return this.callMethod('filters.create', msg)
        },
        removeFilter(_, id) {
          return this.callMethod('posts.remove', id)
        },
        upsertFilter(state, id) {
          return this.callMethod('filters.upsert', state.groups)
        }
  };

  export default {
    state,
    mutations,
    getters,
    actions
  }
