// import {StoreSubModule} from 'meteor/akryum:vuex';
//
// const subModule = new StoreSubModule('bazaar');
//
// const sortFields = ['date', 'name'];
//
// subModule.addState({
//   sortDirection: -1,
//   sortField: sortFields[0]
// });
//
// subModule.addGetters({
//   sortDirection: state => state.sortDirection,
//   sortField: state => state.sortField
// });
//
// subModule.addMutations({
//   FORUM_SORT_DIRECTION(state, direction) {
//     state.sortDirection = direction;
//   },
//   FORUM_SORT_FIELD(state, field) {
//     state.sortField = field;
//   }
// });
//
// subModule.addActions({
//   toggleSortDirection({store, state}) {
//     // state is immutable
//     store.dispatch('FORUM_SORT_DIRECTION', -1*state.sortDirection);
//   },
//   cycleSortField({store, state}) {
//     // state is immutable
//     let index = sortFields.indexOf(state.sortField) + 1;
//     if(index === sortFields.length) {
//       index = 0;
//     }
//     store.dispatch('FORUM_SORT_FIELD', sortFields[index]);
//   }
// });
//
// // Meteor integration
//
// // Import a meteor collection
//
//
// //what does this really do in optimization
// //inspired by Threads
// subModule.addTrackers({
//   selectedFilter() {
//     let sub;
//     let filters;
//     return {
//       init(data) {
//         data.selectedFilters = null;
//         data.listings = [];
//       },
//       activate() {
//         filters = Meteor.subscribe('filters')
//       },
//       deactivate() {
//         sub.stop();
//       },
//       watch(state) {
//         // Dynamic subscription
//         if(sub) {
//           sub.stop();
//         }
//         if(state.selectedFilters) {
//           sub = Meteor.subscribe('bazaarList');
//           console.log('subscribed listings to thread ', state.selectedFilters);
//         }
//
//         return {
//           id: state.selectedFilters
//         }
//       },
//       update(data, {filter}) {
//         data.selectedFilter = Object.freeze(AuctionFilters.find({
//           _id: filter
//         }));
//         data.listings = Object.freeze(AuctionItems.find({
//           charInfo: {skillList: {typeName: filter.name
//           //, level: {$or: [levels]}
//         }
//       }}, {
//           sort: {created: -1}
//         }).fetch());
//         console.log('listings', data.listings);
//       },
//       getters: {
//         getSelectedFilter: data => data.selectedFilter,
//         getListings: data => data.listings
//       }
//     }
//   }
// })
//
//
// // Nested Submodule
// import filter from './filter.js';
// subModule.addModule(filter);
//
// export default subModule;
