
UserPresence.onUserOnline(function(userId){
    //update the profile for the user and set it's status to online
    //works, update users auctions
    var a = Meteor.users.findOne(userId)
    var d = new Date()
    d.setDate(d.getDate-5)
    if (typeof a.auctions != "undefined") {
      var b = AuctionItems.update({_id: {$in: [a.auctions]}, active: true, lastUpdateAuc: {$gte: d}}, {$set: {lastUpdateAuc: new Date()}})
  }
});

// Define a rule that matches login attempts by non-admin users.
const generalRate = {
    type: 'method',
};

// const exampleRate = {
//     type: 'method',
//     name: 'makeBid',
// }
// Add the rule, allowing up to 2 messages every 1000 milliseconds.
DDPRateLimiter.addRule(generalRate, 1, 500);


DDPRateLimiter.setErrorMessage(({ timeToReset }) => {
    const time = Math.ceil(timeToReset / 1000);
    const seconds = time === 1 ? 'second' : 'seconds';
    return `Easy on the gas, buddy. Too many requests. Try again in ${time} ${seconds}.`;
});


AuctionItems._ensureIndex({skills: 1, SPtoISK: -1})
AuctionItems._ensureIndex({name: 1, SPtoISK: -1})
AuctionItems._ensureIndex({lastUpdateAuc: -1, SPtoISK: -1})
AuctionItems._ensureIndex({topBid: 1, SPtoISK: -1})
AuctionItems._ensureIndex({SPtoISK: -1, SPtoISK: -1})
AuctionItems._ensureIndex({totalSP: -1, SPtoISK: -1})
AuctionItems._ensureIndex({active: 1, SPtoISK: -1})


SkillCount._ensureIndex({name: 1})
SkillCount._ensureIndex({index: 1})

Applications._ensureIndex({concluded: 1, recepient: 1}, {sparse: true})
Meteor.users._ensureIndex({roles: 1}, {sparse: true})

Corps._ensureIndex({name: 1})

DeniedCorp._ensureIndex({year: 1, month: 1, day: 1})
HomeCounts._ensureIndex({name: 1})

// {skills: {$in: [{Armor Layering: {$in: [level: 1]}}]}}
// db.auctionItems.aggregate(
//   [
//     {
//       $project: {
//         skillList: 1,
//       }
//     },
//     {
//       $unwind: "$skillList"
//     },
//     {
//       $group: {
//         _id: {type: "$skillList.typeName",level: "$skillList.level",
//         members: {$push: "$_id"},
//         count: {$sum: 1}
//       }
//     },
//   ]
// )
//
//
// {
//   $project: {
//     skillList: 1,
//   }
// },


// var a,b,c,d
// var x =[]
// for (a = 10; a !=0; a--){
// x.push(a)
// for (b = 10; b !=0; b--){
// x.push(b+a)
// for (c = 10; c !=0; c--){
// x.push(c+b+a)
// for (d = 10; d !=0; d--){
// x.push(d+c+b+a)
// }
// }
// }
// }
// var z ={}
//
// var q = x.length
// for (var w=0; q!=w; w+=1) {
// z[x[w]] +=1
// }
// var b = []
// for (sd in z) {
// b.push({key: sd, count: z[sd]})
// }
// b.sort(function (x, y) {
// return x-y})
