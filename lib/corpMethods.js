import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

Meteor.methods({
  'refreshToken': function(user, char){
    if(Meteor.isClient){
      return
    }
    return EveonlineHelpers.refreshAuthToken(user)
  },
  'corp.create': async function(forms, name, skills) {
    //check that one of chars is owner/Director
    //and that api keys check out
    var roles = Swag("Character/get_characters_character_id_roles", {character_id: forms.selected}, Meteor.user())
      if (roles.data.indexOf("director")==-1) {
        throw new Meteor.Error(403, "login with your director character")
      }

    Roles.addUsersToRoles(this.userId, ["Member", "FC", "Recruiter", "Director", "CEO"], name);


    var d = new Date(); //minus 90 days from now, so that task runner can update this
    d.setDate(d.getDate()-90);
    HomeCounts.upsert({name: "corps"}, {$inc: {count: 1}})

    Corps.insert({
      // might need 2 formats, CS for highlight applicants and encoded for search
      // or just only let apply people that match
      ...forms,
      owner: this.userId,
      name: name,
      averageDate: 0, //processing applications
      countDate: 0,
      limit: 20, //number of simultaneous apps
      denyRate: 0, //from aggregation
      updatedDeny: new Date(), //shoudn't be here, should happen after aggregation
      updatedSkills: d,
      accepted: [],
      total: [],
    })

    Meteor.call("corp.updateFilters", skills, name)

  },
  'corp.roles.add': function(index, id, corp) {
    var b = ["Member", "FC", "Recruiter", "Director", "CEO"]
    var a = ["CEO", "Director", "Recruiter", "FC", "Member"]

    var user = Meteor.user()
    //can't demote oneself or mote to outranking
    if (!user || !Roles.userIsInRole(user, b.slice(index), corp)|| user._id == id) {
      throw new Meteor.Error(403, "Access denied")
    }
    Roles.setUserRoles(id, a.slice((1+index)*-1), corp)
  },
  'corp.kick': function (app, corp, cause) {
    //Click aggregator for denial rate
    //click aggregator for acceptance rate
    var user = Meteor.user()
    var actual = Applications.findOne({_id: app})
    var roles = Meteor.users.findOne({_id: actual.owner}).roles[corp].length

    //can't kick outranking
    if (!user || !Roles.userIsInRole(user, ['Recruiter'], corp)|| user.roles[corp].length < roles) {
      throw new Meteor.Error(403, "Insufficient Priviliges")
    }

    var roles = ["Member", "FC", "Recruiter", "Director", "CEO"]
    Roles.removeUsersFromRoles(actual.owner, roles, corp)

    Applications.update(
      {_id: app, "addressees.name": corp},
      { $set: { "addressees.$.status": false, "addressees.$.kicked": redactedUser, "addresses.$.cause": cause }  }
    )

    return
  },
  'corp.updateFilters': function (groups, corp){
    var user = Meteor.user()
    if (!user || !Roles.userIsInRole(user, ['Recruiter'], corp)) {
      throw new Meteor.Error(403, "Insufficient Priviliges")
    }
    var a = Combinations.find({name: {$regex: groups}}).fetch()
    Corps.update({name: corp}, {$set: {skills: groups}})
  },
})

//possibly delete any application references
//schema so that {} doesn't get passed deleting all
export const removeCorp = new ValidatedMethod({
  name: 'corp.Remove',
  validate: new SimpleSchema({
    corpId: { type: String },
    name: { type: String},
  }).validator(),
  run({ corpId, name }) {
    if (Roles.userIsInRole(this.userId, "CEO", name)) {
      Corps.remove(todoId);
    } else {
      throw new Meteor.Error(403, "Insufficient Priviliges")
    }
  }
});

// export const refreshToken = new ValidatedMethod
