/*****************************************************************************/
/* EditCorp: Event Handlers */
/*****************************************************************************/
Template.corporateEdit.events({
});

/*****************************************************************************/
/* EditCorp: Helpers */
/*****************************************************************************/
Template.corporateEdit.helpers({
    beforeRemove: function () {
        return function (collection, id) {
            var doc = collection.findOne(id);
            if (confirm('Really delete corp: "' + doc.brand + " " + doc.model + '"?')) {
                this.remove();
                Router.go('corpsList');
            }
        };
    }
});
