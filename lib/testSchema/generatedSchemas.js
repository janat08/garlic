import idName from '/lib/testSchema/idNameRankAttr.js'
export const idNameObj = idName.reduce((a,x,i)=>{
  a[x.typeID]=x.typeName
  return a
}, {})
