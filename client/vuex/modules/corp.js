import indexes from '/lib/testSchema/indexObj.js'
import indexesArrLen from '/lib/testSchema/array.js'

const state = {
  newGroup: {},
  groups: [{name: "new0", selected: false, skills: {}}, {name: "ike", selected: true, skills: {
    "Acceleration Control": {levels: {1: true}},
    // "Yan Jung Technology": {levels: {1: true}},
    Gunnery: {levels: {1: true}},
    // Accounting: {levels: {1: true}}
}}],
  // collection: [],
  newSkills: [],
  target: 0,

};

const getters = {
  combinedSelected: (state, getters) => {
    var a = getters.groups.filter((x)=>{
      return x.selected
    }).reduce((a, c)=>{
      return Object.assign(a.skills, JSON.parse(JSON.stringify(c.skills)))
    }, {skills: {}})
    for (let c in a.skills){
      let ab
      for (let d in a.skills[c].levels){
        var ac = a.skills[c].levels[d]
        if (d == "n"){
          delete ac
        } else if (typeof ab == "undefined"){
          ab = d
        } else if (ac < ab){
          delete ac
        } else if (ab < ac){
          delete a.skills[c].levels[ab]
          ab = ac
        }
      }
    }
    Session.set('combinedSelected', a)
    Session.set('init', true)
    return a
  },
  selectedFilters: (state, getters) => {
    var abc= 0
    var CS = getters.combinedSelected
    var CSK = Object.keys(CS).sort((x,y)=>{return x.localeCompare(y)})
    for (let a=0; a<CSK.length; a++) {
      CSK[a] = {name: CSK[a], levels: CS[CSK[a]].levels}
    }
    var string = "^"
    for (let a in CSK) {
      // let segment = a!=0?indexes[CSK[a-1].name]:-1
      // string+= ".{"+(indexes[CSK[a].name]-1-segment)+"}(["

      let segment = Math.abs(abc-indexes[CSK[a].name])
      string+= ".{"+segment+"}(["
      abc+= segment+1
      let unTrue= ""
      for (let b in CSK[a].levels){
        if(CSK[a].levels[b] == true) {
          string+=b
        } else {
          unTrue+=b
        }
      }
      if (unTrue.length) {
        string+= "]|[^"+unTrue+"])"
      } else {
        string+= "])"
      }
    }
    //add last regex to fit into whole string
    // console.log(abc,abc+CSK.length, indexes, CSK, indexesArrLen.length, indexes[CSK[CSK.length-1].name])
    string+= ".{"+(indexesArrLen.length-abc)+"}$"
    Session.set('currentF', string)
    return string
  },
  toggle_level_state: (state, getters) => {
    var pl = []
    for (let a in getters.groups) {
      pl.push(JSON.parse(JSON.stringify(getters.groups[a])))
      for (let b in getters.groups[a].skills) {
        var scaffold = [1,2,3,4,5,0,"n"]
        for (let q in scaffold) {
          pl[a].skills[b].levels[scaffold[q]] = {}
          //indeterminate is false, and false is indeterminate
          if (typeof getters.groups[a].skills[b].levels[scaffold[q]] == "undefined") {
            pl[a].skills[b].levels[scaffold[q]].value = "U"
            pl[a].skills[b].levels[scaffold[q]].class = "yellow"
          } else if (getters.groups[a].skills[b].levels[scaffold[q]]) {
            pl[a].skills[b].levels[scaffold[q]].value = "Y"
            pl[a].skills[b].levels[scaffold[q]].class = "green"
          } else {
            pl[a].skills[b].levels[scaffold[q]].value = "N"
            pl[a].skills[b].levels[scaffold[q]].class = "red"
          }
        }
      }
    }
    return pl
  },
  // BUG may not be quite necessery since even if its Reactive
  // it shouldn't push updates
  // decoupled_groups: (state,getters) => {
  //   var a = this.getters.groups.slice()
  // },
};

const mutations = {
  toggle_select(state, index) {
    getters.groups[index].selected = !getters.groups[index].selected
    },
    // FILTER_UNSELECT(state, filter) {
    //   state.selectedFilters =
    //   state.selectedFilters.filter(function(value) {
    //     return value != filter
    //   })
    // },
    add_group (state, name, filters) {
      state.target = 0
      state.shift({name: a, selected: false, skills: {}})
    },
    rename_group (state, pl) {
      getters.groups[state.target].name = pl
    },
    toggle_level (state, pl) {
      let index = pl.index, skill = pl.skill, level = pl.level, value = pl.value;
      //acts on behalf of skill viewer too
      console.log(check)
      if (value=="U") {
        getters.groups[index].skills[skill].levels[level] = "Y"
      } else if (value == "Y") {
        getters.groups[index].skills[skill].levels[level] = "N"
      } else {
        delete getters.groups[index].skills[skill].levels[level]
      }
    },
    add_skill (state, text) {
      getters.groups[state.target].skills[text] = {levels: {}}
    },
    delete_skill (state, pl) {
      console.log(pl)
      delete getters.groups[pl.index].skills[pl.skill]
    },
    delete_group(state, pl) {
      getters.groups.splice(pl, 1)
    },
    push_group(state, pl){
      getters.groups.shift(getters.groups.splice(pl.index,pl.index+1))
    },
  };

  const actions = {
    updateCorpFilters(state, getters, pl) {
      var b = getters.selectedFilters

      return this.callMethod('saveCorpFilters', getters.groups, pl.name)
    },
  };

  const supply = {supply: {
  use: ['Filters'],
  inject: ({ Filters }) => ({
    state: {
      'groups': () => Filters,
    },
  }),
},
}

  export default {
    state,
    getters,
    mutations,
    actions,
  };
