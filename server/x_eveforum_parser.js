import cheerio from 'cheerio';
import parseEveBoard from './z_eveboard_parser.js'
import {encode, deleteAuc, insertAuc} from '/lib/helpers.js'

// parseEveForum(); //IMPORTANT IMPORTANT
// Non REFRACTORED- autionitems constantly populated and deleted, while eveboard parsing runs constantly
//or infact only runs once
//Make sure to sort out duplicates for if for safety users decide to post in both, and conduct business on eve

//check if eveboard value is null in case its a false post
// var object = parseEveBoard("Towaoc")
// encode(object)

function processEveForumTopicRow($, row) {
	if ($(row).hasClass('sticky')) return null;
	var link = $(row).children('.topicMain').children('.maintopic-content').children('a');
	var title = link.text().trim();
	var author = $(row).children('.topicAuthor').children('.auth').children('a').text().trim()
	var url = 'https://forums.eveonline.com' + link.attr('href');
	var lastPosted = $(row).children('.topicLastPost').html().split('<br>')[0];
	lastPosted = Date.parse(lastPosted);
	var object = parseEveBoard(author)
	if (Object.keys(object).length == 0) {
		return lastPosted
	}
	var encoded = encode(object)
	var check = AuctionItems.findOne({name: author})

	if (typeof check != "undefined") {
		if (encoded != check.skills && check.url != url) {
			deleteAuc(check.skills)
			insertAuc(encoded)
			AuctionItems.update({name: author}, {$set: {skills: encoded, url: url}})
		} else if (encoded != check.skills) {
			deleteAuc(check.skills)
			insertAuc(encoded)
			AuctionItems.update({name: author}, {$set: {skills: encoded}})
		} else if (check.url != url) {
			AuctionItems.update({name: author}, {$set: {url: url}})
		}
	} else {
		insertAuc(encoded)
		AuctionItems.insert({
			parsed: true,
			name: author,
			url: url,
			title: title,
			lastPosted: lastPosted,
			skills: encoded
		});
	}
	return lastPosted;
}

function processEveForumPageContents($) {
	var lastPosted = Date.parse('2017.05.21 00:01');
	$('.topicRow').each(function(index, value) {
		var last = processEveForumTopicRow($, value);
		if (last != null) {
			if (last > lastPosted) {
				lastPosted = last;
			}
		}
	});
	$('.topicRow_Alt').each(function(index, value) {
		var last = processEveForumTopicRow($, value);
		if (last != null) {
			if (last > lastPosted) {
				lastPosted = last;
			}
		}
	});
	console.log(lastPosted.toString(), new Date(lastPosted));
	return lastPosted;
}

function parseEveForumPage(page) {
	var url = 'https://forums.eveonline.com/default.aspx?g=topics&f=277&p=' + page;
	var result = HTTP.call('GET', url);
	$ = cheerio.load(result.content);
	return processEveForumPageContents($);
}

function parseEveForum() {
	var page = 1;
	var endDate = new Date();
	endDate.setDate(endDate.getDate() - 2);
	while (parseEveForumPage(page) > endDate) {
		page = page + 1;
	}
	AuctionItems.find({parsed: true, lastPosted: {$lt: endDate}}).fetch().forEach((v)=> {
		deleteAuc(v.skills)
	});
	AuctionItems.remove({parsed: true, lastPosted: {$lt: endDate}});
}
