//https://blog.meteor.com/create-a-simple-hello-world-app-with-meteor-and-apollo-64bab66a456f#.mq12ycxgk

//http://graphql.org/learn/schema/#interfaces
//type certificate/item/skill implements searchable {
// id
// name
// }


export default typeDefs = [`

type Skill {
  typeId: Int
  typeName: String
  rank: Int
  requiredLevel: Int
  requiredSkills: [Skill]
}

type Query {
  ping(message: String): String,
  skill(typeName: String): [Skill],
  findSkill(id: Int): [Skill],
  hello: String
}

schema {
  query: Query
}
`];
