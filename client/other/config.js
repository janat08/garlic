subsCache = new SubsCache({
  expireAfter: 15,
  cacheLimit: -1
});

Vue.config.meteor.subscribe = function(...args) {
  return subsCache.subscribe(...args);
};

// AccountsTemplates.configure({
//   // Appearance
//   showAddRemoveServices: true,
// });

Tracker.autorun(function(){
  Meteor.subscribe('userData', Meteor.userId())
})
