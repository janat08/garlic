Vue.directive('toggleButton', {
  inserted: function (el) {
    $(el)
    .sidebar({
      context: $('#app')
    })
    .sidebar('setting', 'transition', 'overlay')
  }
})

Vue.directive('openTab', {
  bind: function (el, binding) {
    el.addEventListener("click", function(event){
      var win = window.open(binding.value, '_blank');
    })
  }
})
