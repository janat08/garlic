//change meteor.user to this user id other than in
//the api related stuff where it may cause bugs
//unideal async from meteorhacks
import schema from './testSchema/array.js'
import {encode, insertAuc, deleteAuc} from '/lib/helpers.js'
import {idNameObj} from '/lib/testSchema/generatedSchemas.js'

Meteor.methods({
	"sellChar": async function (params) {
		var user = Meteor.user()
		var rawSkills = await Swag("Skills/get_characters_character_id_skills", {character_id: user.profile.eveOnlineCharacterId}, user)
		if (typeof user == "undefined" || typeof rawSkills == "undefined"){
			throw new Meteor.Error(333, "Log in with eve online SSO")
		}
		var skills = encode(rawSkills.skills.reduce((a, x)=>{
			a[idNameObj[x.skill_id]] = x.current_skill_level;
			return a}, {}))
		insertAuc(skills)
		var check = AuctionItems.findOne({name: params.name})
		if (typeof check != "undefined") {
			AuctionItems.update(check._id, {$set: {active: false}})
			deleteAuc(check.skills)
		}
		//parsed/non-parsed diffs by topBid
		if (typeof params.initialPrice == "undefined" || params.initialPrice == 0){
			params.initialPrice = 2
		}
		var a = AuctionItems.insert({
					active: true,
					userID: user._id,
					name: params.name,
					topBid: params.initialPrice,
					title: params.title,
					lastUpdateAuc: new Date(),
					created: new Date(),
					skills: skills,
					totalSP: rawSkills.total_sp,
					SPtoISK: rawSkills.total_sp/params.initialPrice,
				});
		Meteor.users.update(user._id, {$push: {auctions: {id: a, date: new Date()}}})
		return "complete"
	},
	"makeBid": function(params) {
		var a = Meteor.user()
		if (!a || !a.services || !a.services.eveonline.refreshToken) {
			throw new Meteor.Error(403, 'Login with Eve Online SSO');
		}

		var item = AuctionItems.findOne(params.auctionID);
		if (item == undefined) {
			throw new Meteor.Error(404, 'Auction not found');
		}
		var parsed = parseInt(params.value)
		Meteor.users.update({_id: this.userId}, {$push: {bids: {id: params.auctionId, value: parsed, date: new Date()}}})
		var update = {
			$push: {bids: {char: a.profile.eveOnlineCharacterName, user: this.userId, value: parsed, date: new Date()}}
		}
		if (!item.topBid && item.topBid < parsed) {
			update["$set"] = {topBid: parsed, ISKtoSP: 1000000000*parsed/item.totalSP}
		}
		AuctionItems.update({_id: params.auctionId}, update)
	},
	"activate": function(param){
		var a = Meteor.user().auctions.find((x)=>{return x._id == param})
		console.log("activate", a)
		if (typeof a == "undefined"){
			throw new Meteor.Error(123, "Not ur auction")
		}
		AuctionItems.update(param, {$set: {lastUpdateAuc: new Date(), active: true}})
	},
	"deactivate": function(param){
		var a = Meteor.user().auctions.find((x)=>{return x._id == param})
		console.log("activate", a)
		if (typeof a == "undefined"){
			throw new Meteor.Error(123, "Not ur auction")
		}
		AuctionItems.update(param, {$set: {active: false}})
	},
	"saveAuctionFilters": function(param){
		Meteor.users.update(this.userId, {$set: {filters: param}})
	}
});
//
// const LISTS_METHODS = _.pluck([
//     saveAuctionFilters,
//     makePublic,
//     makePrivate,
//     updateName,
//     remove,
// ], 'name');
// // Only allow 5 list operations per connection per second
// if (Meteor.isServer) {
//     DDPRateLimiter.addRule({
//         name(name) {
//             return _.contains(LISTS_METHODS, name);
//         },
//         // Rate limit per connection ID
//         connectionId() { return true; }
//     }, 5, 1000);
// }