import indexes from '/lib/testSchema/indexObj.js'
import indexesArrLen from '/lib/testSchema/array.js'
import corp from './modules/corp.js'

const state = {
  groups: [{name: 1, selected: false, skills:{"Acceleration Control": {levels: {1: true}}}},{name: "ike", selected: true, skills: {
      "Acceleration Control": {levels: {1: true}},
      // "Yan Jung Technology": {levels: {1: true}},
      Gunnery: {levels: {1: true}},
      // Accounting: {levels: {1: true}}
  }}],
  // [],
  // collection: [],
};

// Tracker.autorun(function(){
//   var a = Meteor.users.find(Meteor.userId(),{fields: {filters: 1}})
//   if (Meteor.user() != null || Meteor.user() != "undefined"){
//     Vue.set(state, groups, Meteor.user().filters)
//   } else {
//     state.groups = [{name: "name", selected: false, skills: {}}]
//   }
// })

const getters = {
  combinedSelected: (state, getters) => {
    console.log(state)

    var a = state.groups.slice(1, state.groups.length).filter((x)=>{
      return x.selected
    }).reduce((a, c)=>{
      return Object.assign(a.skills, c.skills)
    }, {skills: {}})
    Session.set('combinedSelected', a)
    Session.set('init', true)
    return a
  },
  selectedFilters: (state, getters) => {
    var abc= 0
    var CS = getters.combinedSelected
    var CSK = Object.keys(CS).sort((x,y)=>{return x.localeCompare(y)})
    for (let a=0; a<CSK.length; a++) {
      CSK[a] = {name: CSK[a], levels: CS[CSK[a]].levels}
    }
    var string = "^"
    for (let a in CSK) {
      // let segment = a!=0?indexes[CSK[a-1].name]:-1
      // string+= ".{"+(indexes[CSK[a].name]-1-segment)+"}(["

      let segment = Math.abs(abc-indexes[CSK[a].name])
      string+= ".{"+segment+"}(["
      abc+= segment+1
      let unTrue= ""
      for (let b in CSK[a].levels){
        if(CSK[a].levels[b] == true) {
          string+=b
        } else {
          unTrue+=b
        }
      }
      if (unTrue.length) {
        string+= "]|[^"+unTrue+"])"
      } else {
        string+= "])"
      }
    }
    //add last regex to fit into whole string
    // console.log(abc,abc+CSK.length, indexes, CSK, indexesArrLen.length, indexes[CSK[CSK.length-1].name])
    string+= ".{"+(indexesArrLen.length-abc)+"}$"
    Session.set('currentF', string)
    return string
  },
  toggle_level_state: (state, getters) => {
    var pl = []
    for (let a in state.groups) {
      pl.push(JSON.parse(JSON.stringify(state.groups[a])))
      for (let b in state.groups[a].skills) {
        var scaffold = [1,2,3,4,5,0,"n"]
        for (let q in scaffold) {
          pl[a].skills[b].levels[scaffold[q]] = {}
          //indeterminate is false, and false is indeterminate
          if (typeof state.groups[a].skills[b].levels[scaffold[q]] == "undefined") {
            pl[a].skills[b].levels[scaffold[q]] = {value: undefined, icon: "radio icon", class: ""}
          } else if (state.groups[a].skills[b].levels[scaffold[q]]) {
            pl[a].skills[b].levels[scaffold[q]] = {value: true, icon: "plus icon", class: "positive"}
          } else {
            pl[a].skills[b].levels[scaffold[q]] = {value: false, icon: "minus icon", class: "negative"}
          }
        }
      }
    }
    return pl
  },
  myShit: (state,getters)=>{
    if (state.groups.length == 0){

    }
    return Meteor.user()
  }
  // BUG may not be quite necessery since even if its Reactive
  // it shouldn't push updates
  // decoupled_groups: (state,getters) => {
  //   var a = this.state.groups.slice()
  // },
};

const mutations = {
  toggle_select(state, index) {
    state.groups[index].selected = !state.groups[index].selected
    },
    // FILTER_UNSELECT(state, filter) {
    //   state.selectedFilters =
    //   state.selectedFilters.filter(function(value) {
    //     return value != filter
    //   })
    // },
    add_group (state, pl) {
      state.groups.unshift({name: "name"+state.groups.length, selected: false, skills: {}})
    },
    rename_group (state, pl) {
      state.groups[0].name = pl
    },
    toggle_level (state, pl) {
      let index = pl.index, name = pl.name, level = pl.level, value = pl.value;
      //acts on behalf of skill viewer too
      if (value== undefined) {
        // state.groups[index].skills[skill].levels[level]
        var a = {}
        a[level] = true
        state.groups[index].skills[name].levels = {...state.groups[index].skills[name].levels, ...a}
      } else if (value) {
        state.groups[index].skills[name].levels[level] = false
      } else {
        Vue.delete(state.groups[index].skills[name].levels, level)
      }
    },
    add_skill (state, text) {
      Vue.set(state.groups[0].skills, text, {levels: {}})
    },
    delete_skill (state, pl) {
      console.log(pl)
      delete state.groups[pl.index].skills[pl.skill]
    },
    delete_group(state, pl) {
      state.groups.splice(pl, 1)
      if (state.groups.length == 0){

      }
    },
    push_group(state, pl){
      state.groups.unshift(state.groups.splice(pl.index, 1)[0])
      // state.groups.push(state.groups.slice(1, 2)[0])
    },
  };

  const actions = {
    // selectFilters({store}, id) {
    //   // state is immutable
    //   store.dispatch('FORUM_SELECTED_THREAD_ID', id);
    // },
    // createFilter(_, name) {
    //   return this.callMethod('filters.create', name, (err, result) => {
    //     if(err) {
    //       console.error(err);
    //     } else {
    //       // Call another action on the submodule
    //       this.actions.selectFilter(result);
    //     }
    //   });
    // },
    removeFilter (state) {
      return this.callMethod('filters.remove', state.selectedFilterId);
    },
    createFilter (_ , msg) {
      return this.callMethod('filters.create', msg)
    },
    updateCorpFilters(state, getters, id) {
      var a = state.groups.reduce((a, x, i)=>{}, {})
      var b = JSON.parse(JSON.stringify(getters.combinedSelected))
      var c = {}
      for (let d of b){

      }

      return this.callMethod('saveCorpFilters', state.groups)
    },
    updateAuctionFilters({state}) {
      return this.callMethod('saveAuctionFilters', state.groups)
    },
    addSaveFilters({commit,dispatch}){
      commit("add_group")
      dispatch("updateAuctionFilters")
    }
  };
//   const supply = {supply: {
//   use: ['Filters'],
//   inject: ({ Filters }) => ({
//     getters: {
//       'groups': () => Filters,
//     },
//   }),
// },
// }


  export default {
    state,
    getters,
    mutations,
    actions,
  };
