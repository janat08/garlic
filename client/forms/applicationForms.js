export default {
about: {
  type: "textarea",
  label: "Expectation from corp:",
  value: ""
},
ships: {
  type: "textarea",
  label: "Favourite ships:"
  ,value: ""
},
activity: {
  type: "textarea",
  label: "How active you're and how you spent your time in past"
  ,value: ""
},
personal: {
  type: "textarea",
  label: "Personal bio; your personality and character:"
  ,value: ""
},
meta: {
  type: "textarea",
  label: "Meta bio; what you have to offer:"
  ,value: ""
},
corps: {
  type: "textarea",
  label: "Corp biography; how you relate to your past corps"
  ,value: ""
},
fights: {
  type: "textarea",
  label: "Combat biography; your best moments and attitute"
  ,value: ""
},
characters: {
  type: "textarea",
  label: "Character biography; overview of your characters",
  value: ""
}
}
