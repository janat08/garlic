import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import Vuex from 'vuex';

// import Vue from 'vue/dist/vue.min.js'

import routerFactory from './routes.js'
import VuexStore from '../vuex/store.js'

import VueCookie from 'vue-cookie'
import VueSupply from 'vue-supply'
import Accounts from 'meteor/accounts-base'

import VueMeteorTracker from 'vue-meteor-tracker';
Vue.use(VueMeteorTracker);

// import SuiVue from 'sui-vue';
// Vue.use(SuiVue);

// import Vuetify from 'vuetify'
// Vue.use(Vuetify)
// import VTooltip from 'v-tooltip'
// Vue.use(VTooltip)

const store = new Vuex.Store(VuexStore);

// router.beforeEach((to, from, next) => {
//   // http://www.regotcha.com/post/vue-js-route-guarding
//   // https://router.vuejs.org/en/advanced/navigation-guards.html
//
//   next(Meteor.subscribe().ready())
// })

import AppLayout from '/imports/ui/AppLayout.vue';

import { ApolloClient, createNetworkInterface } from 'apollo-client'
import VueApollo from 'vue-apollo'


//
// var b = client.apis.Character.get_characters_character_id_roles(
//   {user_agent:"jey.and.key@gmail.com", character_id: 93204672, token: "F1IwRdRYZfQJAG1bdCzpQO_dY6FpOlCqfQCTjl_AWGPA-G_iTD9ODkDjwnTh4fHZ6ZDdH-3ij7DlIm9QlLnF9A2"})
// .catch(c=>{if (c.response.indexOf("expired")){
// if (Meteor.isDevelopment) {
// document.oncontextmenu = function clicky(e){
// 	let vue = e.target.__vue__
// 	if(vue){
// 		console.group(vue.$options.name)
// 		console.log('vue', _.clone(vue))
// 		console.log('data', _.isEmpty(vue._data) ? null :_.clone(vue._data))
// 		let computed = {}
// 		_.each(vue._computedWatchers, (watcher, key) => computed[key] = watcher.value)
// 		console.log('computed', _.isEmpty(computed) ? null :computed)
// 		console.log('props', _.isEmpty(vue._props) ? null : _.clone(vue._props))
// 		console.groupEnd()
// 	} else if(e.target.parentNode){
// 		clicky({target:e.target.parentNode})
// 	}
// }
// }

subsCache = new SubsCache({
  expireAfter: 15,
  cacheLimit: -1
});

Vue.config.meteor.subscribe = function(...args) {
  return subsCache.subscribe(...args);
};
//makes sure sub in vue is ready, calls next to proceed

import SuiVue from 'sui-vue';
Vue.use(SuiVue);

Meteor.startup(() => {
  // const apolloClient = new ApolloClient({
  //   networkInterface: createNetworkInterface({
  //     uri: 'http://localhost:3000/graphql',
  //     transportBatching: true,
  //   }),
  //   connectToDevTools: true,
  // })
  router = routerFactory.create()

  // const apolloProvider = new VueApollo({
  //   defaultClient: apolloClient,
  // })

  router.beforeEach((to, from, next) => {
    subsCache.onReady(next)
})

// router.afterEach((to, from) => {
//   if (from.name == "auction" || from.name == "charInfo") {
//     if (to.name != "auction" || to.name != "charInfo") {
//       Session.set("search", false)
//     }
//   }
// })

  new Vue({
    router,
    store,
    //why applayout gets destructured?
    ...AppLayout,
  }).$mount('app');

  Vue.use(VueApollo)
  Vue.use(VueCookie);
  Vue.use(VueSupply)
});
export {subsCache}
