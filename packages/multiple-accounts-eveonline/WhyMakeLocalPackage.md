Scope access isn't supported with with ui.config so that login control flow templates are automated. Theres programatic option only, which means you need to understand hooks and stuff.


PROGRAMMATIC METHOD FOR INITIATING LOGIN
 Meteor.loginWithEveonline({scope:'characterAccountRead corporationMembersRead esi-mail.send_mail.v1 esi-skills.read_skills.v1 esi-skills.read_skillqueue.v1 esi-wallet.read_character_wallet.v1 esi-characters.read_corporation_roles.v1'})


SSO PARAMS INTO ACCOUNTS-UI PACKAGE
// Accounts.ui.config({
//   requestPermissions: {
//     eveonline: ['characterAccountRead corporationMembersRead esi-mail.send_mail.v1 esi-skills.read_skills.v1 esi-skills.read_skillqueue.v1 esi-wallet.read_character_wallet.v1 esi-characters.read_corporation_roles.v1']
//   }
// })
