//https://blog.meteor.com/create-a-simple-hello-world-app-with-meteor-and-apollo-64bab66a456f#.mq12ycxgk
import Sequelize from 'sequelize';

// create the connection
// the new pass is j
const db = new Sequelize('sde', "root", null, {
    host: 'localhost',
    dialect: 'mysql'
});

// define the model
const SkillModel = db.define('skillList', {
    typeId: {type: Sequelize.INTEGER, primaryKey: true},
    typeName: { type: Sequelize.STRING },
    rank: {type: Sequelize.INTEGER}
}, {
    timestamps: false
});

const InvTypes = db.define('invTypes', {
  typeId: {type: Sequelize.INTEGER, primaryKey: true},
  typeName: {type: Sequelize.STRING }
}, {
  timestamps: false
});

const TypeAttributes = db.define('dgmTypeAttributes', {
  typeID: {type: Sequelize.INTEGER, primaryKey: true},
  attributeID: {type: Sequelize.INTEGER }
}, {
  timestamps: false
});


// create the table if it doesn't exist yet
// db.sync();

// export Post
const Skill = db.models.skillList;
const Inv = db.models.invTypes;
const Type = db.models.dgmTypeAttributes;
export { db, Skill, Inv, Type };
