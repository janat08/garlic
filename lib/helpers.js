import schemaIndexes from "/lib/testSchema/indexObj.js"
import Swagger from 'swagger-client'
import schema from "/lib/testSchema/array.js"

// Swag("Character/get_characters_character_id", {character_id: 95465499}).then(x=>{
//   console.log(x)
// })

Swag = async function asdf(api, target, user) {
  var client = await Swagger('https://esi.tech.ccp.is/latest/swagger.json?datasource=tranquility')
  .then(client => {
    // client.spec // The resolved spec
    // client.originalSpec // In case you need it
    // client.errors // Any resolver errors
    // Tags interface

    return client
  });
  return new Promise(function(resolve, reject) {
    if (typeof user != "undefined" || user.services.eveonline.expiresAt < new Date()) {
      var handle = Meteor.users.find(user._id, {
        fields: {
          "services.eveonline.accessToken": 1
        }
      }).observeChanges({
        changed: function(id, fields) {
          user.services.eveonline["accessToken"] = fields.services.eveonline.accessToken;
          resolve(user); // Refreshed, continue to done
          handle.stop();
        }
      })
      Meteor.call('refreshToken', user)
      setTimeout(()=>{
        handle.stop; reject("Without permissions")
      }, 10000)
    } else {
      resolve(user); // No refresh needed, continue to done
    }
  })
  .then(function(user) {
    var endpoint = api.split("/").reduce((a, x) => {
      return a[x]
    }, client.apis)
    var params = {...target, user_agent: "jey.and.key@gmail.com"
  }
  if (typeof user != "undefined") {
    params["token"] = user.services.eveonline.accessToken
  }
  return endpoint(params)
}).then(function(res){
  return res.obj
})
}

//
// async function testOutdatedToken () {
//   var b = {_id: Meteor.user()._id, services: {eveonline:{accessToken: "vV0ZaFNvgXKM_TCLJ4yRL-7vVDaR4Jz6Jl_xt-kAl-ImePXJJJkTxxqV4xgjuTRww3pGO323IbgyQ67Vnrv9Eg2", expiresAt: new Date().setDate(-1), refreshToken: Meteor.user().services.eveonline.refreshToken}}}
//   console.log("seed", b)
//   var a = await Swag("Character/get_characters_character_id_roles", {character_id: 2112299983}, b).then(x=>console.log("Swag results", x))
//
// }

function encode (skills) {
  return schema.reduce((a, x, i) => {
    var add = typeof skills[x] != "undefined"? skills[x]: "n"
    if (add=="undefined")console.log (skills[x], x, add)
    return a+add
  }, "")
}

function decode (skills) {
  return schema.reduce((a, x, i) => {
    if (skills[i] != "n"){
      a[x] = Number(skills[i])
    }
    return a
  }, {})
}

function insertAuc (string, prevString) {
  //set up prevString for updates of skills
  if (Meteor.isClient) return
  AuctionCount.upsert({name: string}, {$inc: {total: 1}})
  HomeCounts.upsert({name: "auctions"}, {$inc: {count: 1}})
  var arr = string.split('')
  for (var ind in arr) {
    var num = Number(arr[ind])
    var obj = {}
    obj[num] = 1
    SkillCount.update({index: ind}, {$inc: {...obj, total: 1}})
  }
}

function deleteAuc (string, prevString) {
  if (Meteor.isClient) return
  AuctionCount.upsert({name: string}, {$inc: {total: -1}})
  HomeCounts.upsert({name: "auctions"}, {$inc: {count: -1}})
  var arr = string.split('')
  for (var ind in arr) {
    var num = Number(arr[ind])
    var obj = {}
    obj[num] = -1
    SkillCount.update({index: ind}, {$inc: {...obj, total: -1}})
  }
}
//
// if (!!Scaffold.findOne().nexists) {
// var diagram = []
// var length = schema.length*6
// var lengthSchema= schema.length+1 //428+1
//
// function scaffold (state) {
//   if (lengthSchema == state.length) {
//     diagram.push(state)
//     return
//   }
//   scaffold(state+"n")
//   for (let b = 0; b != 5; b++ ) {
//     scaffold(state+b)
//   }
// }
//
// scaffold("")
//
// diagram.sort((x,y)=>{return x.localeCompare(y)})
// //then sort the skill filters
// //then find the first and last eg:
// //find: "1555..." and "1nnn..." if descending
// //then slice those
// //then go down list
// //the same stuff
//
// Scaffold = new Meteor.Collection('scaffold')
// Scaffold.insert({nexists: false, diagram:diagram})
// }
//
// //apears to be wrong
// //if 1st index is set to 3
// //then none of these apply to filter set, or override previous filters on schemaIndexes
// //should instead be simple array.indexOf(x) == skill.level
//
// // SHOULDN'T BE A LOOP, will not respect previous levels 3/2/1 and 4/3/1 at point of 1
// // function checkSkill (skill) {
// //   var name = Object.keys(skill)[0]
// //   var index = schemaIndexes[name]
// //   var range = {top: "", bot: ""}
// //   range.top = "0".repete(index)+skill
// //   range.bot = "n".repete(index)+skill
// //   range.top = diagram.findIndex((x)=>{return x.indexOf(range.top)==0})
// //   range.bot = diagram.findIndex((x)=>{return x.indexOf(range.bot)==0})
// //   return range
// // }
//
// checkNonSkill should operate at the end of array
// should narrow down result sets, then combine them collectively
// sort them and start deleting from the end as to not disturb numbers
// function wrapCheck () {
// var results = diagram.slice()
// function recursiveCheck (index, state, filters ) {
//   if (state.length==schema.length) {
//     results.push(state)
//     return
//   }
//   Object.keys(filters[index]).forEach((x, i, a)=>{
//     var name = Object.keys(x)[0]
//     if (x == true) {
//     state+=Object.keys(x)[0]
//     var top =results.findIndex((z)=>{return z.indexOf(state)==0})
//     var bot =results.lastIndex((z)=>{return z.indexOf(state)==0})
//     result.splice(0, top+1)
//     result.splice(-1, bot+1)
//   }
//     //recursiveCheck(index, state...) etc.
//     //should also check for entries added previously
//   })
// }
// return results
// }
// //export wrapCheck(0, "")
//
// function checkSkill (array) {
//   var length = array.length
//   for (let a = 0; a != length; a++) {
//
//   }
//   var name = Object.keys(skill)[0]
//   var index = schemaIndexes[name]
//   var range = {top: "", bot: ""}
//   range.top = "0".repete(index)+skill
//   range.bot = "n".repete(index)+skill
//   range.top = diagram.findIndex((x)=>{return x.indexOf(range.top)==0})
//   range.bot = diagram.findIndex((x)=>{return x.indexOf(range.bot)==0})
//   return range
// }
//
// //runs first to remove from copy of diagram or last removing from filter set
//
// function checkNonSkill() {
//
// }
//
// function unCheckNonSkill() {
//
// }

export {encode, decode, insertAuc, deleteAuc}
