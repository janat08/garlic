// Meteor.publish('auctionItemsCount', function(query){
//
// 	console.log(query, "AAAAAAAAAAAAAAAAAA")
// 	Counts.publish(this, 'auctionItemsCount.${query}', AuctionItems.find({...query}), {nonReactive: true})
// })
// https://github.com/nate-strauser/meteor-publish-performant-counts/
// MUCH BETTER APPROACH, or basically just keeping count of records
//still good for excat match
var userCount = new Counter("userCount", Meteor.users.find({}, {fields: {profile: 1}}), 2000)
var auctionCount = new Counter("auctionCount", AuctionItems.find({active: true}), 4000)

// Meteor.publish('publication', function() {
//   Counts.publish(this, 'name-of-counter', Posts.find());
// });

Meteor.publish("homeCounts", function(){
  return [
    userCount, auctionCount
  ]
})

Meteor.publish('userData', function() {
  if (!this.userId) return this.stop();
  return Meteor.users.find(this.userId, {fields: {"services.eveonline.refreshToken": 0, "services.password.bcrypt": 0, "services.resume": 0}})
})

Meteor.publish('corpMembers', function(corp) {
	if (!this.userId) return this.stop();
  var a = Meteor.users.findOne({_id:this.userId})
	if (typeof a.roles == "undefined" || typeof a.roles[corp] == "undefined") return this.stop();
	var query = {}
	query["roles."+corp]= {$exists: true}
  var projection = {}
  projection["roles."+corp]=1
	// return Meteor.users.find()
	return Meteor.users.find(query, {fields: projection})
// 		,
// 		{fields: {
// 		apiKeys: 1,
// 		chars: 1,
// 		roles: 1
// 	}}

});

Meteor.publish('auctionCount', function () {
	return AuctionCount.find()
})

Meteor.publish("users", function () {
  var user = Meteor.users.findOne({_id:this.userId});
//there's new mechanic for having admin group and role
  if (Roles.userIsInRole(user, ["admin","manage-users"])) {
    console.log('publishing users', this.userId)
    return Meteor.users.find({}, {fields: {emails: 1, profile: 1, roles: 1}});
  }

  this.stop();
  return;
});

Meteor.publish("corpRecords", function (corp) {
  var a = Meteor.users.findOne(this.userId).roles
  if (Roles.userIsInRole(this.userId, "Recruiter", corp)) {
    return Applications.find({recepient: corp}, {})
  } else {
    return Applications.find({recepient: corp}, {fields: {secretApiKeys: 0}})
  }
  Applications.find({recepients})
})

Meteor.publish('corps', function () {
  return Corps.find();
});

Meteor.publish('charsInfo', function() {

	return CharsInfo.find();
});

Meteor.publish('allCharsInfo', function(id) {
	if (!this.userId) return null;
	return CharsInfo.find();
});

Meteor.publish('bazaarList', function() {
	//if query is large enforce limit.
	return AuctionItems.find();
});

// Meteor.publish('listingBids', function() {
// 	if (!this.userId) return null;
// 	return AuctionBids.find();
// });

Meteor.publish('filters', function() {
	return AuctionFilters.find();
})

Meteor.publish('deniedCorp', function() {
  var a = new Date()
  a.setDate(-30)
  return DeniedCorp.find({year: {$gte: a.getFullYear()}, month: {$gte: a.getMonth()}, day: {$gte: a.getDate()}})
})






// DONT DO THIS, EXPOSES SPIES
// Meteor.publish(null, function (){
//   return Meteor.roles.find({})
// })
