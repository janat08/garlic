Template.corporate.onCreated(function() {
  this.subscribe("userData")
})

Template.corporate.helpers({
roles: function () {
  if (!this.roles) {
    return '<none>'
  }

  return this.roles.join(',')
},
users: function () {
  return Meteor.users.find()
},

})
