import cheerio from 'cheerio'
//refractor foreach with filter since there're tests toward the end
//data: array [name, rank, level]



function parseEveBoard(pilot) {
  var url = 'http://eveboard.com/pilot/'+ pilot.replace(' ', '_'); //or instead of pilot the full url var scraped from eveforum
  var result = HTTP.call('GET', url);
  var eveB = cheerio.load(result.content);
  return buildObject(eveB)
  //possibly... this.buildArray(eveB) instead

}

function buildObject(eveB) {
  var skills = {}
  eveB('td.dotted').filter(function() {
    var data=eveB(this)
    var skill = []
    data.text().split('/').forEach(function(content, index){
      if (index < 3 && content.length > 1) {
        skill.push(content.trim().replace(/Rank|Level:/, ''))
      }
    }
  )
  if (skill.length > 1){
    var index = skill[0].trim()
    skills[index] = Number(skill[2].trim())
  }
})

return skills
}

// var q = parseEveBoard
// export default q;

//////////////////////////////////////////////////
// function processEveForumTopicRow($, row) {
//   if ($(row).hasClass('sticky')) return null;
//   var link = $(row).children('.topicMain').children('.maintopic-content').children('a');
//   var title = link.text().trim();
//   var url = 'https://forums.eveonline.com' + link.attr('href');
//   var lastPosted = $(row).children('.topicLastPost').html().split('<br>')[0];
//   lastPosted = Date.parse(lastPosted);
//   AuctionItems.upsert({parsed: true, url: url}, {
//     parsed: true,
//     url: url,
//     title: title,
//     lastPosted: lastPosted
//   });
//   return lastPosted;
// }
//
// function processEveForumPageContents($) {
//   var lastPosted = Date.parse('2000.01.01 00:01');
//   $('.topicRow').each(function(index, value) {
//     var last = processEveForumTopicRow($, value);
//     if (last != null) {
//       if (last > lastPosted) {
//         lastPosted = last;
//       }
//     }
//   });
//   $('.topicRow_Alt').each(function(index, value) {
//     var last = processEveForumTopicRow($, value);
//     if (last != null) {
//       if (last > lastPosted) {
//         lastPosted = last;
//       }
//     }
//   });
//   console.log(lastPosted.toString());
//   return lastPosted;
// }
//
// function parseEveForumPage(page) {
//   var url = 'https://forums.eveonline.com/default.aspx?g=topics&f=277&p=' + page;
//   var result = HTTP.call('GET', url);
//   $ = cheerio.load(result.content);
//   return processEveForumPageContents($);
// }
//
// function parseEveForum() {
//   var page = 1;
//   var endDate = new Date();
//   endDate.setDate(endDate.getDate() - 2);
//   while (parseEveForumPage(page) > endDate) {
//     page = page + 1;
//   }
//   AuctionItems.remove({parsed: true, lastPosted: {$lt: endDate}});
// }
//
// }
