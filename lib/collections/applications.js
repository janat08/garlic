Applications = new Mongo.Collection('applications');
Applications.deny({
    insert() { return true; },
    update() { return true; },
    remove() { return true; },
});
/// depending on what category is chosen some forms should hide/appear
/*
who vouches for you
option for describing why join this corp// or
expectation from corp
your favorite ship
your past leadership experience
your past experience with corps
your best fight
about you
overview of characters along with list of all chars
activity level
and favorite activity

wh probe scanning probes/directional

corp form
ur legend
ur form
ur pitch

*/
var scaffold = {
  about: {
    type: String,
    label: "Expectation from corp:"
  },
  ships: {
    type: String,
    label: "Favourite ships:"
  },
  activity: {
    type: String,
    label: "How active you're and how you spent your time in past"
  },
  personal: {
    type: String,
    label: "Personal bio; your personality and character:"
  },
  meta: {
    type: String,
    label: "Meta bio; what you have to offer:"
  },
  corps: {
    type: String,
    label: "Corp biography; how you relate to your past corps"
  },
  fights: {
    type: String,
    label: "Combat biography; your best moments and attitute"
  },
  characters: {
    type: String,
    label: "Character biography; overview of your characters"
  }
}
