// import * as tasks from "./tasks.js"
//
// // JobsWorker.initialize({
// //     collectionName: 'JobQueue',
// //     workerInstances: parseInt(process.env.WORKER_INSTANCES || '1'), //worker can only do one type of job
// //     stalledJobCheckInterval: 5 * 60 * 1000, // ms
// //     promoteInterval: 15 * 60 * 1000 // ms
// // });
// //
// // Meteor.startup(function () {
// //     JobsWorker.start()
// // });
// //
// // class Deactivate5DaysOld extends Job {
// //     run() {
// //         var a = new Date()
// //         AuctionItems.update({lastUpdateAuc: {$lte: a.setDate(a.getDate()-5)}}, {active: false})
// //         this.logInfo("Hello world from: " + this.data.name);
// //     }
// // }
// //
// // Deactivate5DaysOld.register();
// //
// // new Deactivate5DaysOld({name: "Foo"}).enqueue({repeat: {wait: 3000000}});
// //
// // // var myJobs = JobCollection('myJobQueue');
// // //
// // Meteor.startup(function () {
// //   // Normal Meteor publish call, the server always
// //   // controls what each client can see
// //
// //   // Start the myJobs queue running
// //   return myJobs.startJobServer();
//
//
// Meteor.startup(function () {
//     console.log(123123123123123)
//     JC = JobCollection('myJobQueue');
//     JC.promote(1000) //polling rate
//
// //IMPORTANT, grants client admin rights
//     JC.allow({
//         // The "admin" below represents
//         // the grouping of all remote methods
//         admin: function (userId, method, params) {
//             return true;
//         }
//     });
//
//     JC.deny({
//         // The "admin" below represents the
//         // grouping of all remote methods
//         admin: function (userId, method, params) {
//             return true;
//         }
//     });
//
//   // Normal Meteor publish call, the server always
//   // controls what each client can see
//   // Meteor.publish('allJobs', function () {
//   //   return JC.find({});
//   // });
//
//   // Start the JC queue running
//   //  JC.startJobServer();
//
//
//     var job = new Job(JC, 'genericJob', // type of job
//         // Job data that you define, including anything the job
//         // needs to complete. May contain links to files, etc...
//         {
//             spec: "clearOutdatedAuctions"
//         }
//     )
//         job.priority('high')
//         .repeat({
//             wait: 60 * 1000   // minute between each re-run.
//         })
//         .save();            // Commit it to the server
//
// //     new Job(JC, 'test', // type of job
// // // Job data that you define, including anything the job
// // // needs to complete. May contain links to files, etc...
// //         {
// //             // date: new Date(),
// //         }
// //     ).priority('high')
// //         .repeat({
// //             wait: 1000   // minute between each re-run.
// //         }).retry({
// //         retries: 10,
// //         wait: 1000,
// //     })
// //         .save();            // Commit it to the server
//
// //     var jq = JC.processJobs(
// //         // 'myJobQueue',
// //         ['genericJob', "test"],
// //         {
// //             concurrency: 2,
// //             payload: 1,
// //             pollInterval: 1000 * 60 * 60 * 24,
// //             workTimeout: 3000,
// //         },
// //         function (job, cb) {
// //             var jobType
// //             switch (job.data.spec) {
// //                 case "clearOutdatedAuctions":
// //                     jobType = tasks.clearOutdatedAuctions;
// //                     break;
// //                 case "test":
// //                     jobType = tasks.test;
// //                     break;
// //             }
// //             jobType(job)
// //             job.done()
// //             cb()
// //
// //            // var a = new Promise(function (resolve) {
// //            //          var jobType
// //            //          switch (job.type) {
// //            //              case "clearOutdatedAuctions":
// //            //                  jobType = clearOutdatedAuctions;
// //            //                  break;
// //            //              case "test":
// //            //                  jobType = test;
// //            //                  break;
// //            //          }
// //            //          resolve(
// //            //              jobType(job)
// //            //          )
// //            //      }
// //            //  )
// //            //  a.then(() => {
// //            //      job.done()
// //            //      return cb()
// //            //  })
// //         }
// //     );
// //
// //     JC.find({status: 'ready'}) //no job type
// //         .observe({
// //             added: function () {
// //                 jq.trigger();
// //             }
// //         });
// // })
//
// ///cleanup jobs///
// //
// // new Job(JC, 'cleanup', {})
// // .repeat({ schedule: JC.later.parse.text("every 5 minutes") })
// // .save({cancelRepeats: true});
// //
// // let q = JC.processJobs('cleanup', { pollInterval: false, workTimeout: 60*1000 }, function(job, cb) {
// //   let current = new Date();
// //   current.setMinutes(current.getMinutes() - 5);
// //   let ids = JC.find({ status: {$in: [Job.jobStatusRemovable]}, updated: {$lt: current}}, {fields: { _id: 1 }})
// //   .map(d => d._id);
// //   if (ids.length > 0) { JC.removeJobs(ids); }
// //   // console.warn "Removed #{ids.length} old jobs"
// //   job.done(`Removed ${ids.length} old jobs`);
// //   return cb();
// // }
// // );
// //
// // return JC.find({ type: 'cleanup', status: 'ready' })
// // .observe({
// //   added() {
// //     return q.trigger();
// //   }
// // });
